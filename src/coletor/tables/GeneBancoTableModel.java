package coletor.tables;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

import coletor.banco.GeneBanco;
import coletor.com.IdGene;
import coletor.telas.Idioma_Language;

public class GeneBancoTableModel extends AbstractTableModel {	 
 
    //lista dos genes que ser�o exibidos
    private List<GeneBanco> genes;
    private List<GeneBanco> modificados = new ArrayList();
    private Idioma_Language idioma;
 
    public  GeneBancoTableModel(Idioma_Language idioma) {
        genes = new ArrayList();        	    
        this.idioma = idioma;
    }
 
    public GeneBancoTableModel(List<GeneBanco> lista, Idioma_Language idioma) {      
        genes = lista;
        this.idioma = idioma;
    }
 
    public int getRowCount() {
        //cada produto na lista ser� uma linha
        return genes.size();
    }

    public int getColumnCount() {
        //vamos exibir s� Nome e Quantidade, ent�o s�o 3 colunas
        return 24;
    }
 
    @Override
    public String getColumnName(int column) {
        //qual o nome da coluna
        if (column == 0) {
            return idioma.getTableResultados_1();
        }

        if (column == 1) {
            return idioma.getTableResultados_2();
        }

        if (column == 2) {
            return idioma.getTableResultados_3();
        }

        if (column == 3) {
            return idioma.getTableResultados_4();
        }

        if (column == 4) {
            return idioma.getTableResultados_5();
        }

        if (column == 5) {
            return idioma.getTableResultados_6();
        }
        
        if (column == 6) {
            return idioma.getTableResultados_7();
        }

        if (column == 7) {
            return idioma.getTableResultados_8();
        }
        
        if (column == 8) {
            return idioma.getTableResultados_9();
        }
        
        if (column == 9) {
            return idioma.getTableResultados_10();
        }
        
        if (column == 10) {
            return idioma.getTableResultados_11();
        }
        
        if (column == 11) {
            return idioma.getTableResultados_12();
        }
        
        if (column == 12) {
            return idioma.getTableResultados_13();
        }
        
        if (column == 13) {
            return idioma.getTableResultados_14();
        }
        
        if (column == 14) {
            return idioma.getTableResultados_15();
        }
        
        if (column == 15) {
            return idioma.getTableResultados_16();
        }
        
        if (column == 16) {
            return idioma.getTableResultados_17();
        }
        
        if (column == 17) {
            return idioma.getTableResultados_18();
        }
        
        if (column == 18) {
            return idioma.getTableResultados_19();
        }
        
        if (column == 19) {
            return idioma.getTableResultados_20();
        }
        
        if (column == 20) {
            return idioma.getTableResultados_21();
        }
        
        if (column == 21) {
            return idioma.getTableResultados_22();
        }
        
        if (column == 22) {
            return idioma.getTableResultados_23();
        }
        
        if (column == 23) {
            return idioma.getTableResultados_24();
        }
        
        return "";
    }
 
    @Override
    public Class getColumnClass(int columnIndex) {
        //retorna a classe que representa a coluna     
        return String.class;
    }
 
    public Object getValueAt(int rowIndex, int columnIndex) {
        //pega o produto da linha
    	GeneBanco p = genes.get(rowIndex);
    	
    	//System.out.println("Organismo <"+p.getAlsoKnownAs());
 
        //verifica qual valor deve ser retornado
        if (columnIndex == 0) {
            return p.getOfficialSymbol();
        } 
        
        if (columnIndex == 1) {
            return p.getGeneId();
        } 
        
        if (columnIndex == 2) {
            return p.getAlsoKnownAs();
        } 

        if (columnIndex == 3) {
            return p.getOfficialName();
        } 
        
        if (columnIndex == 4) {
            return p.getLocationBanda();
        } 
        
        if (columnIndex == 5) {
            return p.getReferences();
        } 
        
        if (columnIndex == 6) {
            return p.getTranscripts();
        } 
        
        if (columnIndex == 7) {
            return p.getLocationCoodernada();
        } 
        
        if (columnIndex == 8) {
            return p.getTypeStrand();
        } 
        
        if (columnIndex == 9) {
            return p.getGeneType();
        } 
        
        if (columnIndex == 10) {
            return p.getIdEnsembl();
        } 
        
        if (columnIndex == 11) {
            return p.getMetodoAnalise();
        } 
        
        if (columnIndex == 12) {
            return p.getSexo();
        } 
        
        if (columnIndex == 13) {
            return p.getIdade();
        } 
        
        if (columnIndex == 14) {
            return p.getnIndividuos();
        } 
        
        if (columnIndex == 15) {
            return p.getEndurance();
        } 
        
        if (columnIndex == 16) {
            return p.getAgudoCronico();
        } 
        
        if (columnIndex == 17) {
            return p.getUpDown();
        }

        if (columnIndex == 18) {
            return p.getOrganism();
        } 
        
        if (columnIndex == 19) {
            return p.getSequence();
        } 
        
        if (columnIndex == 20) {
            return p.getGeneType2();
        } 
        
        if (columnIndex == 21) {
            return p.getName();
        }

        if (columnIndex == 22) {
            return p.getDescription();
        } 
        
        if (columnIndex == 23) {
            return p.getSinonyms();
        } 
        
        return "";
    }
 
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        //pega o produto da linha
    	GeneBanco p = genes.get(rowIndex);
    	    	     	                  
         
        //verifica qual valor vai ser alterado
        
        if (columnIndex == 0) {        	
             p.setOfficialSymbol(aValue.toString());
            
        } 
        
        if (columnIndex == 1) {
             p.setGeneId(aValue.toString());
             
        } 
        
        if (columnIndex == 2) {
             p.setAlsoKnownAs(aValue.toString());
          
        } 

        if (columnIndex == 3) {
             p.setOfficialName(aValue.toString());
             
        } 
        
        if (columnIndex == 4) {
             p.setLocationBanda(aValue.toString());
             
        } 
        
        if (columnIndex == 5) {
        	
            p.setReferences(aValue.toString());
            
        } 
        
        if (columnIndex == 6) {
             p.setTranscripts(aValue.toString());
             
        } 
        
        if (columnIndex == 7) {
             p.setLocationCoodernada(aValue.toString());
             
        } 
        
        if (columnIndex == 8) {
             p.setTypeStrand(aValue.toString());
             
        } 
        
        if (columnIndex == 9) {
             p.setGeneType(aValue.toString());
             
        } 
        
        if (columnIndex == 10) {
             p.setIdEnsembl(aValue.toString());
             
        } 
        
        if (columnIndex == 11) {
            p.setMetodoAnalise(aValue.toString());
            
       } 
        
        if (columnIndex == 12) {
            p.setSexo(aValue.toString());
           
       } 
          
        if (columnIndex == 13) {
            p.setIdade(aValue.toString());
           
       } 
 
        if (columnIndex == 14) {
            p.setnIndividuos(aValue.toString());           
       } 
        
        if (columnIndex == 15) {
            p.setEndurance(aValue.toString());           
       } 
        
        if (columnIndex == 16) {
            p.setAgudoCronico(aValue.toString());            
       } 
        
        if (columnIndex == 17) {
            p.setUpDown(aValue.toString());            
       } 

        if (columnIndex == 18) {
            p.setOrganism(aValue.toString());
        } 
        
        if (columnIndex == 19) {
            p.setSequence(aValue.toString());
        } 
        
        if (columnIndex == 20) {
            p.setGeneType2(aValue.toString());
        } 
        
        if (columnIndex == 21) {
            p.setName(aValue.toString());
        }

        if (columnIndex == 22) {
            p.setDescription(aValue.toString());
        } 
        
        if (columnIndex == 23) {
            p.setSinonyms(aValue.toString());
        } 
        
        //adicionando p a lista de genes modificados para alterar no banco de dados.
        modificados.add(p);
        //avisa que os dados mudaram        
        fireTableDataChanged();
    }
 
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        //no nosso caso todas v�o ser edit�veis, entao retorna true pra todas
        return true;
    }
 
 
    public void inserir(GeneBanco p) {
        genes.add(p);
     
        fireTableDataChanged();
    }
     
    public void excluir(int pos) {
        genes.remove(pos);
     
        fireTableDataChanged();
    }
    
    public void excluir(IdGene p) {
        genes.remove(p);
     
        fireTableDataChanged();
    } 

    public GeneBanco getGene(int pos) {    
        if(pos >= 0 && pos< genes.size())
        return genes.get(pos);
        
        return null;
    }
    
    public ArrayList ListGeneID() {
    	
		return (ArrayList) genes;
    	
    }
    
    
    public void setList(ArrayList<GeneBanco> l){
    	
    	genes = l;
    	
    }
    
    public ArrayList<GeneBanco>  getListModificaos(){
    	return (ArrayList) modificados;
    }
    
    public void clearModificados(){
    	
    	modificados = new ArrayList(); 
    }

	/**
	 * @return the idioma
	 */
	public Idioma_Language getIdioma() {
		return idioma;
	}

	/**
	 * @param idioma the idioma to set
	 */
	public void setIdioma(Idioma_Language idioma) {
		this.idioma = idioma;
	}
    
   
 }