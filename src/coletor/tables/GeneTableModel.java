package coletor.tables;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import coletor.com.IdGene;
import coletor.telas.Idioma_Language;


public class GeneTableModel extends AbstractTableModel {
	 
   //constantes que v�o representar as colunas
   //(s� para facilitar o entendimento do c�digo)
   private final int COL_NOME  = 0;
   private final int COL_QUANT = 1;
   private Idioma_Language idioma;

   public List<IdGene> getGenes() {
	   return genes;
	}
	
	public void setGenes(List<IdGene> genes) {
		this.genes = genes;
	}

//lista dos produtos que ser�o exibidos
   private List<IdGene> genes;

   public  GeneTableModel(Idioma_Language idioma) {
       genes = new ArrayList();
       this.idioma = idioma;
   }

   public GeneTableModel(List<IdGene> lista,Idioma_Language idioma) {
       genes=lista;
       this.idioma = idioma;
   }

   public int getRowCount() {
       //cada produto na lista ser� uma linha
       return genes.size();
   }

   public int getColumnCount() {
       //vamos exibir s� Nome e Quantidade, ent�o s�o 3 colunas
       return 3;
   }

   @Override
   public String getColumnName(int column) {
	   
       //qual o nome da coluna
	      if (column == 0) {
	           return idioma.getTableConjuntoGenes_1();
	       } 
	       
	       if (column == 1) {
	           return idioma.getTableConjuntoGenes_2();
	       }
	       
	       if (column == 2) {
	           return idioma.getTableConjuntoGenes_3();
	       }
	       return "";
   }

   @Override
   public Class getColumnClass(int columnIndex) {
       //retorna a classe que representa a coluna
       if (columnIndex == 0) {
           return String.class;
       } else if (columnIndex == 1) {
           return String.class;
       }
       return String.class;
   }

   public Object getValueAt(int rowIndex, int columnIndex) {
       //pega o produto da linha
   	IdGene p = genes.get(rowIndex);

       //verifica qual valor deve ser retornado
       if (columnIndex == 0) {
           return p.getGeneId();
       } 
       else if (columnIndex == 1) {
           return p.getIdEnsembl();
       }else if (columnIndex == 2){
       	  
       	  return p.getWhatdo();
       }
       return "";
   }

   @Override
   public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
       //pega o produto da linha
       IdGene p = genes.get(rowIndex);

       //verifica qual valor vai ser alterado
       if (columnIndex == 0) {
           p.setGeneId(aValue.toString());
       } 
       
       if (columnIndex == 1) {
           p.setIdEnsembl(aValue.toString());
       }

       if (columnIndex == 2) {
       	
       	 p.setWhatdo(aValue.toString());
       }

       //avisa que os dados mudaram
       fireTableDataChanged();
   }

   @Override
   public boolean isCellEditable(int rowIndex, int columnIndex) {
       //no nosso caso todas v�o ser edit�veis, entao retorna true pra todas
       return false;
   }


   public void inserir(IdGene p) {
       genes.add(p);
    
       fireTableDataChanged();
   }
    
   public void excluir(int pos) {
       genes.remove(pos);
    
       fireTableDataChanged();
   }
   
   public void excluir(IdGene p) {
       genes.remove(p);
    
       fireTableDataChanged();
   } 

   public IdGene getGeneID(int pos) {
       if (pos < 0 || pos >= genes.size()) {
           return null;
       }
    
       return genes.get(pos);
   }
   
   public ArrayList ListGeneID() {
   	
		return (ArrayList) genes;
   	
   }

/**
 * @return the idioma
 */
public Idioma_Language getIdioma() {
	return idioma;
}

/**
 * @param idioma the idioma to set
 */
public void setIdioma(Idioma_Language idioma) {
	this.idioma = idioma;
}
}