package coletor.sintese;

public class Chromosome {
	
	private String valor;
	private int  quantidade;
	
	public Chromosome(String valor){
		
		this.valor = valor;
		quantidade = 1;		
	}
	
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	public int getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

}
