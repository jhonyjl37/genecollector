package coletor.sintese;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import jxl.write.WritableSheet;
import coletor.banco.GeneBanco;
import coletor.com.PlanilhaBanco;

public class Sintese {
	
	private int li=0; // linha atual
    
	ArrayList<GeneBanco> listaGenes;
	public ArrayList<GeneBanco> getListaGenes() {
		return listaGenes;
	}


	public void setListaGenes(ArrayList<GeneBanco> listaGenes) {
		this.listaGenes = listaGenes;
	}


	ArrayList<Info>  listInfoGeneType = new ArrayList<Info>();
	ArrayList<Info>  listInfoOrgarnism = new ArrayList<Info>();
	
		
	public void run(WritableSheet sheet,PlanilhaBanco pan){
		
	
		//Fazer isso para todos os genes da lista
		for(GeneBanco g: listaGenes){
			
			if(g.getGeneType().length()>0)
			//comparar gene g com todos os elementos da lista listInfo
		   if(compararValorGeneType(g) == false){
			   //caso se falso adiciona valor a lista
			   Info in = new Info(g.getGeneType());			    			
			   listInfoGeneType.add(in);
		   }
						
		}
				
		
		//Fazer isso para todos os genes da lista
		for(GeneBanco g: listaGenes){
			
			if(g.getOrganism().length()>0)
			//comparar gene g com todos os elementos da lista listInfo
		   if(compararValorOrganism(g) == false){
			   //caso se falso adiciona valor a lista
			   
			    Info in = new Info(g.getOrganism());
			    String sequence = g.getSequence();
			    int fim = sequence.indexOf("(");
			    sequence = sequence.substring(0,fim);
			   
			   in.getListChromo().add(new Chromosome(sequence));
			   
			   listInfoOrgarnism.add(in);
			   
		   }
						
		}
		
		try{
			
		     Date agora = new Date();;  
			SimpleDateFormat formata = new SimpleDateFormat("dd/MM/yyyy");  
			String data1 = formata.format(agora);  
			formata = new SimpleDateFormat("h:mm - a");  
			String hora1 = formata.format(agora);
			
		
		pan.addCaption(sheet, 0, li,  "	Gene set summary created by Gene Collector v1.0, on "+data1+" ("+hora1+")" );
		li++;
		pan.addLabel(sheet, 0, li,  "" );
		li++;
		
		pan.addCaptionLeft(sheet, 0, li,  "Gene type:" );
		li++;
		pan.addLabel(sheet, 0, li,  "" );
		li++;
		}catch(Exception e) {}
       
		for(Info i: listInfoGeneType){			
			
			try{
				pan.addLabel(sheet, 0, li,  i.getQuantidade()+" "+i.getValor() );
				li++;
			
				}catch(Exception e) {}
			
		}
				
		

		try{
			pan.addLabel(sheet, 0, li,  "" );
			li++;
			pan.addCaptionLeft(sheet, 0, li, "Organisms:");
			li++;
			pan.addLabel(sheet, 0, li,  "" );
			li++;
			}catch(Exception e) {}		
				
		for(Info i: listInfoOrgarnism){			
			
			try{
				pan.addLabel(sheet, 0, li, i.getQuantidade()+" "+i.getValor());
				li++;
			
				}catch(Exception e) {}		
			
		}
		
		try{
			pan.addLabel(sheet, 0, li,  "" );
			li++;
			pan.addCaptionLeft(sheet, 0, li,"Chromosome distribution:");
			li++;
		
			}catch(Exception e) {}	
		
		
		for(Info i: listInfoOrgarnism){
			
			try{
				pan.addLabel(sheet, 0, li,  "" );
				li++;
				pan.addLabel(sheet, 0, li,i.getValor());
				li++;			
				}catch(Exception e) {}	
		   				
		    	i.toChromo(sheet,pan,li);
		}
	}
	
	
	// M�todo que compara gene g com todos os elementos da lista listInfo
   private boolean compararValorGeneType(GeneBanco g){
	
		for(Info i: listInfoGeneType){
			 
			//Verificando se valor j� existe na lista
			if(g.getGeneType().equals(i.getValor())){
				
				//Aumentando a quantidade desse valor 
				i.setQuantidade(i.getQuantidade()+1);
				return true;
			}
				
			}
		
	   return false;
   }
   
   private boolean compararValorOrganism(GeneBanco g){
		
		for(Info i: listInfoOrgarnism){
			 
			//Verificando se valor j� existe na lista
			if(g.getOrganism().equals(i.getValor())){
				
				//Aumentando a quantidade desse valor 
				i.setQuantidade(i.getQuantidade()+1);
				
				if(compararValorChrome(g, i.getListChromo()) == false ){
					
					
					    String sequence = g.getSequence();
					    int fim = sequence.indexOf("(");
					    sequence = sequence.substring(0,fim);					   
					   i.getListChromo().add(new Chromosome(sequence));
				}
				
				return true;
			}				
			}
		
	   return false;
   }
   
   
	// M�todo que compara gene g com todos os elementos da lista listInfo
   private boolean compararValorChrome(GeneBanco g,ArrayList<Chromosome> chro){
	   
	    String sequence = g.getSequence();
	    int fim = sequence.indexOf("(");
	    sequence = sequence.substring(0,fim);
	   
		for(Chromosome c: chro){
			 
			//Verificando se valor j� existe na lista
			if(sequence.equals(c.getValor())){
				
				//Aumentando a quantidade desse valor 
				c.setQuantidade(c.getQuantidade()+1);
				return true;
			}
				
			}
		
	   return false;
   }
	
}
