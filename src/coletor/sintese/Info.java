package coletor.sintese;

import java.util.ArrayList;

import jxl.write.WritableSheet;
import coletor.com.PlanilhaBanco;

public class Info {
	
	private String valor;
	private int  quantidade;
	private ArrayList<Chromosome> listChromo;
	
	public Info(String valor){
	  
		this.valor= valor;
		quantidade = 1;
		listChromo= new ArrayList<Chromosome>();
	}
	
	public ArrayList<Chromosome> getListChromo() {
		return listChromo;
	}

	public void setListChromo(ArrayList<Chromosome> listChromo) {
		this.listChromo = listChromo;
	}

	public String getValor() {
		return valor;
		
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	public int getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}
	
	public void toChromo(WritableSheet sheet, PlanilhaBanco pan,int li){

		
		for(Chromosome cr : listChromo){
			
			try{
				pan.addLabel(sheet, 0,li, cr.getQuantidade()+" in "+cr.getValor());
				li++;			
				}catch(Exception e) {}	
			
	
		
		}
		
		
	}
	
	
	
}
