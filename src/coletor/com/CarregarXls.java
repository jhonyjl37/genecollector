package coletor.com;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JFileChooser;
import javax.swing.UIManager;
import javax.swing.filechooser.FileNameExtensionFilter;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class CarregarXls {

 private String path;

 /*
  * Construtor recebi como par�metro o caminho do arquivo xls.
  */
 public CarregarXls(){
	 
	 path= getPath();	 	 	 	
 }
 
 /*
  * M�todo que ler o aqruivo xls e retorna uma lista de IdGene.
  */
 public ArrayList<IdGene> getLitaGeneId(){
	
	 if (path == null)
		 return null;
	 
	 ArrayList<IdGene> listaId= new ArrayList<IdGene>();
	 
	 /**
      * Carrega a planilha
      */

      Workbook workbook = null;
	try {
		workbook = Workbook.getWorkbook(new File(path));		
	} catch (BiffException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

      /**
      * Aqui � feito o controle de qual aba do xls 
      * sera realiza a leitura dos dados
      */

      Sheet sheet = workbook.getSheet(0);

      /**
      * Numero de linhas com dados do xls
      */

      int linhas = sheet.getRows();
      
      for(int i = 0; i < linhas; i++  )

      {

          Cell celula1 = sheet.getCell(0, i);
          Cell celula2 = sheet.getCell(1, i);          
          
          if(!celula1.getContents().equals("") || !celula2.getContents().equals("")){                         
          listaId.add(new IdGene(celula1.getContents(),celula2.getContents()));
          }
          
      }
	 
	 return listaId;	 
 }

 
	private String getPath(){
		 //Alterando o skin da janela save.
		 try {	
	 		   
	 		   UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());                  
       } catch (Exception e) {
           e.printStackTrace();
       }
		 		 		 
		 // Mostrando para o usuario a janela save, para o mesmo esolher o local de destino do arquivo.
		
		    JFileChooser chooser = new JFileChooser ();
		    chooser.setFileSelectionMode(JFileChooser.OPEN_DIALOG);  
	 		FileNameExtensionFilter filter = new FileNameExtensionFilter ("*.xls","xls");  
	 		chooser.addChoosableFileFilter(filter);
	 		chooser.setFileFilter(filter);
	 		chooser.setAcceptAllFileFilterUsed(false); 
	        
	 		int op = chooser.showOpenDialog(chooser);
            if(op == JFileChooser.APPROVE_OPTION){  
                 File arq = chooser.getSelectedFile();  
                 String path = arq.toString();//+".xls";  
                 System.out.println(path);
                 return path;
                                  
            }else{
          	  //Falha ao conseguir o caminho do novo aquivo;
          	  return null;
            }
            
	}
}
