package coletor.com;

public class IdGene {
	
	private String geneId;
	private String idEnsembl;
	private boolean status;
	private String  whatdo="";
	
	public String getWhatdo() {
		return whatdo;
	}



	public void setWhatdo(String whatdo) {
		this.whatdo = whatdo;
	}



	public IdGene (String geneId, String idEnsembl) {
		
		this.geneId= geneId;
		this.idEnsembl= idEnsembl;
		
	}



	public boolean isStatus() {
		return status;
	}



	public void setStatus(boolean status) {
		this.status = status;
	}



	public String getGeneId() {
		return geneId;
	}

	public void setGeneId(String geneId) {
		this.geneId = geneId;
	}

	public String getIdEnsembl() {
		return idEnsembl;
	}

	public void setIdEnsembl(String idEnsembl) {
		this.idEnsembl = idEnsembl;
	}

}
