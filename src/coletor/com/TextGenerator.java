package coletor.com;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class TextGenerator {

	public static boolean writeStringToFile(String path, String data) {
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new FileWriter(path));
			writer.write(data);
		} catch (IOException e) {
			return false;
		}finally
		{
		    try
		    {
		        if ( writer != null)
		        writer.close( );
		    }
		    catch ( IOException e)
		    {
		    	return false;
		    }
		}
		
		return true;
	}

}
