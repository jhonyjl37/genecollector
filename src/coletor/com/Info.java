package coletor.com;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Toolkit;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextPane;

import coletor.telas.FrameWork;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class Info extends JFrame {

	private JPanel contentPane;
	private JLabel lblNambuSoftware;
	private JTextPane txtpnCriadoPelaEmpressa;
	private JLabel lblMes;
	
	/**
	 * Create the frame.
	 */
	public Info() {
		addWindowListener(new WindowAdapter() {
			@Override
			
			public void windowClosing(WindowEvent arg0) {
				setVisible(false);
			}
			
			
		});
		setIconImage(Toolkit.getDefaultToolkit().getImage(Info.class.getResource("/busca/info.png")));
		setResizable(false);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(150, 150, 350, 250);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblNambuSoftware = new JLabel("Gene Collector  V " + FrameWork.getIdioma().getVers�oSoftware());
		lblNambuSoftware.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNambuSoftware.setBounds(97, 11, 128, 27);
		contentPane.add(lblNambuSoftware);
		
		txtpnCriadoPelaEmpressa = new JTextPane();
		txtpnCriadoPelaEmpressa.setText(FrameWork.getIdioma().getInforma��o());
		txtpnCriadoPelaEmpressa.setEditable(false);
		txtpnCriadoPelaEmpressa.setOpaque(false);
		txtpnCriadoPelaEmpressa.setBounds(36, 49, 340, 240);
		contentPane.add(txtpnCriadoPelaEmpressa);
		
		lblMes = new JLabel(FrameWork.getIdioma().getM�s());
		lblMes.setBounds(129, 134, 68, 27);
		contentPane.add(lblMes);
	}

	/**
	 * @return the lblNambuSoftware
	 */
	public JLabel getLblNambuSoftware() {
		return lblNambuSoftware;
	}

	/**
	 * @param lblNambuSoftware the lblNambuSoftware to set
	 */
	public void setLblNambuSoftware(JLabel lblNambuSoftware) {
		this.lblNambuSoftware = lblNambuSoftware;
	}

	/**
	 * @return the txtpnCriadoPelaEmpressa
	 */
	public JTextPane getTxtpnCriadoPelaEmpressa() {
		return txtpnCriadoPelaEmpressa;
	}

	/**
	 * @param txtpnCriadoPelaEmpressa the txtpnCriadoPelaEmpressa to set
	 */
	public void setTxtpnCriadoPelaEmpressa(JTextPane txtpnCriadoPelaEmpressa) {
		this.txtpnCriadoPelaEmpressa = txtpnCriadoPelaEmpressa;
	}

	/**
	 * @return the lblMes
	 */
	public JLabel getLblMes() {
		return lblMes;
	}

	/**
	 * @param lblMes the lblMes to set
	 */
	public void setLblMes(JLabel lblMes) {
		this.lblMes = lblMes;
	}
}
