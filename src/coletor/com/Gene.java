package coletor.com;

public class Gene {
	
	private String officialSymbol;
	private String geneId;
	private String OfficialName;
	private String alsoKnownAs;
	private String locationBanda;
	private String locationCoodernada;
	private String typeStrand; // forward/reverse Strand; 
	private String Transcripts;
	private String GeneType;
	private String idEnsembl;
	//novos atributos adicionados
    private String organism;
    private String sequence;
    private String GeneType2;
    private String name;  
    private String description; 
    private String sinonyms;    
	
	public String getOrganism() {
		return organism;
	}



	public void setOrganism(String organism) {
		this.organism = organism;
	}



	public String getSequence() {
		return sequence;
	}



	public void setSequence(String sequence) {
		this.sequence = sequence;
	}



	public String getGeneType2() {
		return GeneType2;
	}



	public void setGeneType2(String geneType2) {
		GeneType2 = geneType2;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	public String getSinonyms() {
		return sinonyms;
	}



	public void setSinonyms(String sinonyms) {
		this.sinonyms = sinonyms;
	}



	public Gene(String officialSymbol,String geneId,String alsoKnownAs,
			String OfficialName,String locationBanda,String Transcripts,
			String locationCoodernada,String typeStrand,String GeneType,
			String idEnsembl,String organism,String sequence,String GeneType2,	String name,String description,String sinonyms){
		
		 this.officialSymbol = officialSymbol;
		 this.geneId = geneId;		 
		 this.OfficialName = OfficialName;
		 this.alsoKnownAs = alsoKnownAs;
		 this.locationBanda = locationBanda;
		 this.locationCoodernada = locationCoodernada;
		 this.typeStrand = typeStrand; // forward/reverse Strand; 
		 this.Transcripts = Transcripts;
		 this.GeneType = GeneType;
		 this.idEnsembl = idEnsembl;
		 this.organism = organism;
		 this.sequence = sequence;
		 this.GeneType2 = GeneType2;
		 this.name = name;  
		 this.description = description; 
		 this.sinonyms = sinonyms; 
				
	}



	public String getOfficialSymbol() {
		return officialSymbol;
	}

	public void setOfficialSymbol(String officialSymbol) {
		this.officialSymbol = officialSymbol;
	}

	public String getGeneId() {
		return geneId;
	}

	public void setGeneId(String geneId) {
		this.geneId = geneId;
	}

	public String getOfficialName() {
		return OfficialName;
	}

	public void setOfficialName(String officialName) {
		OfficialName = officialName;
	}

	public String getAlsoKnownAs() {
		return alsoKnownAs;
	}

	public void setAlsoKnownAs(String alsoKnownAs) {
		this.alsoKnownAs = alsoKnownAs;
	}

	public String getLocationBanda() {
		return locationBanda;
	}

	public void setLocationBanda(String locationBanda) {
		this.locationBanda = locationBanda;
	}

	public String getLocationCoodernada() {
		return locationCoodernada;
	}

	public void setLocationCoodernada(String locationCoodernada) {
		this.locationCoodernada = locationCoodernada;
	}

	public String getTypeStrand() {
		return typeStrand;
	}

	public void setTypeStrand(String typeStrand) {
		this.typeStrand = typeStrand;
	}

	public String getTranscripts() {
		return Transcripts;
	}

	public void setTranscripts(String transcripts) {
		Transcripts = transcripts;
	}

	public String getGeneType() {
		return GeneType;
	}

	public void setGeneType(String geneType) {
		GeneType = geneType;
	}

	public String getIdEnsembl() {
		return idEnsembl;
	}

	public void setIdEnsembl(String idEnsembl) {
		this.idEnsembl = idEnsembl;
	}


	

}
