package coletor.com;

import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.table.AbstractTableModel;

import coletor.telas.FrameWork;
import coletor.telas.Idioma_Language;
import coletor.telas.SalvarExcel;

public class Principal extends Thread {
	
	ArrayList<IdGene> listaBaixar ;
	SalvarExcel listaGene = new SalvarExcel();
	JButton button;
	JButton buttonCancel;
	
	private Idioma_Language idioma = FrameWork.getIdioma();

	public Principal(ArrayList<IdGene> list,JButton j, JButton c){
		
		listaBaixar= list;
		button = j;
		buttonCancel = c;
	}
	
	 public void run (AbstractTableModel model) { 
		
	    //Baixar info de  todos os genes que foram adicionados na lista Baixar.
		for( IdGene g: listaBaixar){	
			if (  !g.getWhatdo().equals(idioma.getStatus3())){
			
			    g.setWhatdo(idioma.getStatus1());
			    model.fireTableDataChanged();
			    
				g.setStatus(HtmlParserFooter.iniciarColeta(listaGene,g.getGeneId(),g.getIdEnsembl()));
				
				if (g.isStatus() == true){
					g.setWhatdo(idioma.getStatus3());
					model.fireTableDataChanged();
				}
				else{
					g.setWhatdo(idioma.getStatus4());
					model.fireTableDataChanged();
				}
			}			
		}
		
		button.setEnabled(true);
		buttonCancel.setEnabled(false);
	}

	 public void salvar(){

		try {
			listaGene.salvarExcel();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 }
	
}
