package coletor.com;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.filechooser.FileNameExtensionFilter;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.Alignment;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import coletor.banco.GeneBanco;
import coletor.sintese.Sintese;
import coletor.telas.Idioma_Language;


public class PlanilhaBanco extends JFrame {

	private WritableCellFormat timesBoldUnderline;
	private WritableCellFormat timesBoldUnderlineLeft;
	private WritableCellFormat times;
	private String inputArquivo;
	private WritableSheet sheet;
	
	//Lista de Genes
	ArrayList<GeneBanco> listaGenes;
	private Idioma_Language idioma;
	
	public PlanilhaBanco( ArrayList<GeneBanco> l, Idioma_Language idioma){
		listaGenes = l;
		this.idioma = idioma; 
	}
	
	
	public void setOutputFile(String inputArquivo) {
		this.inputArquivo = inputArquivo;
		}
	
	
	public void insere() throws IOException, WriteException {
		// Cria um novo arquivo
		File arquivo = new File(inputArquivo);
		WorkbookSettings wbSettings = new WorkbookSettings();
		 
		wbSettings.setLocale(new Locale("pt", "BR"));
		 
		WritableWorkbook workbook = Workbook.createWorkbook(arquivo, wbSettings);
		// Define um nome para a planilha
		workbook.createSheet("Dados", 0);
		workbook.createSheet("S�ntese", 1);
		sheet = workbook.getSheet(0);
		
		WritableSheet sheet1= workbook.getSheet(1);
		
		criaLabel(sheet);
		
		defineConteudo(sheet);
		criaSintese(sheet1);
		sheet1.setColumnView(0, 400);
		 
		workbook.write();
		workbook.close();
		}
	
	
	// M�todo respons�vel pela defini��o das labels
	private void criaLabel(WritableSheet sheet)	throws WriteException {
	// Cria o tipo de fonte como TIMES e tamanho
	WritableFont times10pt = new WritableFont(WritableFont.ARIAL, 10);
	 
	// Define o formato da c�lula
	times = new WritableCellFormat(times10pt);
	
	
	 
	// Efetua a quebra autom�tica das c�lulas
	times.setWrap(true);
	 
	// Cria a fonte em negrito com underlines
	WritableFont times10ptBoldUnderline = new WritableFont(
	WritableFont.ARIAL, 10, WritableFont.BOLD, false);
	//UnderlineStyle.SINGLE);
	timesBoldUnderline = new WritableCellFormat(times10ptBoldUnderline);
	timesBoldUnderlineLeft = new WritableCellFormat(times10ptBoldUnderline);
	
	 
	timesBoldUnderline.setAlignment(Alignment.CENTRE);
	timesBoldUnderlineLeft.setAlignment(Alignment.LEFT);
	// Efetua a quebra autom�tica das c�lulas
	timesBoldUnderline.setWrap(true);
	timesBoldUnderlineLeft.setWrap(true);
	
	CellView cv = new CellView();
	cv.setFormat(times); //Bom pessoal, � isso ai, qualquer d�vida � s� avisar.
	cv.setFormat(timesBoldUnderline);
	cv.setAutosize(true);

	sheet.setColumnView(0, 25);
	sheet.setColumnView(1, 25);
	sheet.setColumnView(2, 25);
	sheet.setColumnView(3, 50);
	sheet.setColumnView(4, 25);
	sheet.setColumnView(5, 25);
	sheet.setColumnView(6, 25);
	sheet.setColumnView(7, 25);
	sheet.setColumnView(8, 25);
	sheet.setColumnView(9, 25);
	sheet.setColumnView(10, 25);
	sheet.setColumnView(11, 25);
	sheet.setColumnView(12, 25);
	sheet.setColumnView(13, 25);
	sheet.setColumnView(14, 25);
	sheet.setColumnView(15, 25);
	sheet.setColumnView(16, 25);
	sheet.setColumnView(17, 25);
	sheet.setColumnView(18, 25);
	sheet.setColumnView(19, 75);
	sheet.setColumnView(20, 25);
	sheet.setColumnView(21, 25);
	sheet.setColumnView(22, 75);
	sheet.setColumnView(23, 25);
	
	// Escreve os cabe�alhos
	try {
		addCaption(sheet, 0, 1, getIdioma().getTableResultados_1_1());
		addCaption(sheet, 1, 1, getIdioma().getTableResultados_2());
		addCaption(sheet, 2, 1, getIdioma().getTableResultados_3());
		addCaption(sheet, 3, 1, getIdioma().getTableResultados_4());
		addCaption(sheet, 4, 1, getIdioma().getTableResultados_5_1());
		addCaption(sheet, 5, 1, getIdioma().getTableResultados_6());
		addCaption(sheet, 6, 1, getIdioma().getTableResultados_7());
		addCaption(sheet, 7, 1, getIdioma().getTableResultados_8_1());
		addCaption(sheet, 8, 1, getIdioma().getTableResultados_9_1());
		addCaption(sheet, 9, 1, getIdioma().getTableResultados_10_1());
		addCaption(sheet, 10, 1, getIdioma().getTableResultados_11());
		addCaption(sheet, 11, 1, getIdioma().getTableResultados_12());
		addCaption(sheet, 12, 1, getIdioma().getTableResultados_13());
		addCaption(sheet, 13, 1, getIdioma().getTableResultados_14());
		addCaption(sheet, 14, 1, getIdioma().getTableResultados_15_1());
		addCaption(sheet, 15, 1, getIdioma().getTableResultados_16_1());
		addCaption(sheet, 16, 1, getIdioma().getTableResultados_17_1());
		addCaption(sheet, 17, 1, getIdioma().getTableResultados_18_1());
		addCaption(sheet, 18, 1, getIdioma().getTableResultados_19());
		addCaption(sheet, 19, 1, getIdioma().getTableResultados_20());
		addCaption(sheet, 20, 1, getIdioma().getTableResultados_21_1());
		addCaption(sheet, 21, 1, getIdioma().getTableResultados_22());
		addCaption(sheet, 22, 1, getIdioma().getTableResultados_23());
		addCaption(sheet, 23, 1, getIdioma().getTableResultados_24());

	} catch (RowsExceededException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (WriteException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		
	
	}
	 
	private void criaSintese(WritableSheet sheet) throws WriteException,RowsExceededException {
		
			   Sintese s = new Sintese();
			   s.setListaGenes(listaGenes);
			   s.run(sheet,this);
			   
		   
	              
	}
	
	private void defineConteudo(WritableSheet sheet) throws WriteException,RowsExceededException {
 		
		 Date agora = new Date();;  
			SimpleDateFormat formata = new SimpleDateFormat("dd/MM/yyyy");  
			String data1 = formata.format(agora);  
			formata = new SimpleDateFormat("h:mm - a");  
			String hora1 = formata.format(agora);
			
	    
		addCaption(sheet, 0, 0,  "	Table generated by Gene Collector v1.0, on  "+data1+" ("+hora1+")" );
		
		   for(int i= 2; i<listaGenes.size();i++)  {
			  
			   addLabel(sheet, 0, i+1,  listaGenes.get(i).getOfficialSymbol() );
			   addLabel(sheet, 1, i+1,  listaGenes.get(i).getGeneId() );
			   addLabel(sheet, 2, i+1,  listaGenes.get(i).getAlsoKnownAs());
			   addLabel(sheet, 3, i+1,  listaGenes.get(i).getOfficialName());
			   addLabel(sheet, 4, i+1,  listaGenes.get(i).getLocationBanda());
			   addLabel(sheet, 5, i+1,  listaGenes.get(i).getReferences());
			   addLabel(sheet, 6, i+1,  listaGenes.get(i).getTranscripts());
			   addLabel(sheet, 7, i+1,  listaGenes.get(i).getLocationCoodernada());
			   addLabel(sheet, 8, i+1,  listaGenes.get(i).getTypeStrand());
			   addLabel(sheet, 9, i+1,  listaGenes.get(i).getGeneType());
			   addLabel(sheet, 10, i+1, listaGenes.get(i).getIdEnsembl());
			   addLabel(sheet, 11, i+1, listaGenes.get(i).getMetodoAnalise());
			   addLabel(sheet, 12, i+1, listaGenes.get(i).getSexo());
			   addLabel(sheet, 13, i+1, listaGenes.get(i).getIdade());
			   addLabel(sheet, 14, i+1, listaGenes.get(i).getnIndividuos());
			   addLabel(sheet, 15, i+1, listaGenes.get(i).getEndurance());
			   addLabel(sheet, 16, i+1, listaGenes.get(i).getAgudoCronico());
			   addLabel(sheet, 17, i+1, listaGenes.get(i).getUpDown());
			   addLabel(sheet, 18, i+1, listaGenes.get(i).getOrganism());
			   addLabel(sheet, 19, i+1, listaGenes.get(i).getSequence());
			   addLabel(sheet, 20, i+1, listaGenes.get(i).getGeneType2());
			   addLabel(sheet, 21, i+1, listaGenes.get(i).getName());
			   addLabel(sheet, 22, i+1, listaGenes.get(i).getDescription());
			   addLabel(sheet, 23, i+1, listaGenes.get(i).getSinonyms());
		   }   
	              
	}
	 
	// Adiciona cabecalho
	public void addCaption(WritableSheet planilha, int coluna, int linha, String s) throws RowsExceededException, WriteException {
	    Label label;
	    label = new Label(coluna, linha, s, timesBoldUnderline);
	    planilha.addCell(label);
	}
	
	// Adiciona cabecalho
	public void addCaptionLeft(WritableSheet planilha, int coluna, int linha, String s) throws RowsExceededException, WriteException {
	    Label label;
	    label = new Label(coluna, linha, s, timesBoldUnderlineLeft);
	    planilha.addCell(label);
	}
	 
	 
	public void addLabel(WritableSheet planilha, int coluna, int linha, String s) throws WriteException, RowsExceededException {
	   Label label;
	   label = new Label(coluna, linha, s, times);
	   planilha.addCell(label);
	}
	
	
	
	/*
	 * Salvar a lista de genes para um formato do Excel.
	 */	
	public boolean salvarExcel() throws IOException{
		   String path = "";
		   
		   if(listaGenes.size()== 0){
			   
			   //Lista de genes vazia;
			   return false;
		   }
		   
		      //conseguindo o endere�o onde o arquivo ser� salvo.
              path= getPath();
              
              //Criando e salvando arquivo do Excel.
              setOutputFile(path);
              try {
              insere();
              return true;
              } catch (WriteException e) {
              // TODO Auto-generated catch block
              e.printStackTrace();
              } catch (IOException e) {
              // TODO Auto-generated catch block
              e.printStackTrace();
              }
              
              
        return false;
	}
	
	private String getPath(){
		 //Alterando o skin da janela save.
		 try {	
	 		   
	 		   UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());                  
        } catch (Exception e) {
            e.printStackTrace();
        }
		 
		 		 
		 // Mostrando para o usuario a janela save, para o mesmo esolher o local de destino do arquivo.
		
		    JFileChooser chooser = new JFileChooser ();
		    chooser.setFileSelectionMode(JFileChooser.CUSTOM_DIALOG);  
	 		FileNameExtensionFilter filter = new FileNameExtensionFilter ("*.xls","xls");  
	 		chooser.addChoosableFileFilter(filter);
	 		chooser.setFileFilter(filter);
	 		chooser.setAcceptAllFileFilterUsed(false); 
	        
	 		  int op = chooser.showSaveDialog(chooser);  
             if(op == JFileChooser.APPROVE_OPTION){  
                 File arq = chooser.getSelectedFile();  
                  String path = arq.toString()+".xls";  
                  System.out.println(path);
                  return path;
                                   
             }else{
           	  //Falha ao conseguir o caminho do novo aquivo;
           	  return "";
             }
             
	}


	/**
	 * @return the idioma
	 */
	public Idioma_Language getIdioma() {
		return idioma;
	}


	/**
	 * @param idioma the idioma to set
	 */
	public void setIdioma(Idioma_Language idioma) {
		this.idioma = idioma;
	}
	
	public void configurarPlanilha(Idioma_Language idioma){
		this.idioma = idioma;
		// Escreve os cabe�alhos
		try {
			addCaption(sheet, 0, 0, idioma.getTableResultados_1_1());
			addCaption(sheet, 1, 0, idioma.getTableResultados_2());
			addCaption(sheet, 2, 0, idioma.getTableResultados_3());
			addCaption(sheet, 3, 0, idioma.getTableResultados_4());
			addCaption(sheet, 4, 0, idioma.getTableResultados_5_1());
			addCaption(sheet, 5, 0, idioma.getTableResultados_6());
			addCaption(sheet, 6, 0, idioma.getTableResultados_7());
			addCaption(sheet, 7, 0, idioma.getTableResultados_8_1());
			addCaption(sheet, 8, 0, idioma.getTableResultados_9_1());
			addCaption(sheet, 9, 0, idioma.getTableResultados_10_1());
			addCaption(sheet, 10, 0, idioma.getTableResultados_11());
			addCaption(sheet, 11, 0, idioma.getTableResultados_12());
			addCaption(sheet, 12, 0, idioma.getTableResultados_13());
			addCaption(sheet, 13, 0, idioma.getTableResultados_14());
			addCaption(sheet, 14, 0, idioma.getTableResultados_15_1());
			addCaption(sheet, 15, 0, idioma.getTableResultados_16_1());
			addCaption(sheet, 16, 0, idioma.getTableResultados_17_1());
			addCaption(sheet, 17, 0, idioma.getTableResultados_18_1());
			addCaption(sheet, 18, 0, getIdioma().getTableResultados_19());
			addCaption(sheet, 19, 0, getIdioma().getTableResultados_20());
			addCaption(sheet, 20, 0, getIdioma().getTableResultados_21_1());
			addCaption(sheet, 21, 0, getIdioma().getTableResultados_22());
			addCaption(sheet, 22, 0, getIdioma().getTableResultados_23());
			addCaption(sheet, 23, 0, getIdioma().getTableResultados_24());

		} catch (RowsExceededException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (WriteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}