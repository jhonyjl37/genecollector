package coletor.com;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import coletor.telas.SalvarExcel;

public class HtmlParserFooter{

	private Document document;

	static String geneID;
	static String idEnsembl;
	
	static SalvarExcel listaGenes;
	
	static String officialSymbol;
	static String OfficialName;
	static String alsoKnownAs;
	static String locationBanda;
	static String locationCoodernada;
	static String typeStrand;
	static String Transcripts;
	static String GeneType;
	static String organism;
	static String sequence;
	static String GeneType2;
	static String name;  
	static String description; 
	static String synonyms;    



	public  HtmlParserFooter(Document document)
	{
		this.document = document;
	}

	public static boolean iniciarColeta(SalvarExcel listaGenes, String geneID,String idEnsembl)
	{


		 boolean parar ;

		   for(int i= 0; i<20 ; i++){
			System.out.println("tentando : pela "+(i+1));
			//addLista � a fun��o que ir� iniciar a coleta de fato.
			parar = addLista(listaGenes, geneID, idEnsembl);

			if (parar == true ){
				return true;
			}

		 }

		   return false;

	 }

	/*
	 * M�todo respons�vel por conectar com a internet baixar os documents e chamar outros m�todos da classe para coletar as informa��es;
	 */
	private  static boolean addLista(SalvarExcel listaGenes,String geneID,String idEnsembl)
	{


	 officialSymbol = "";
	 OfficialName = "";
	 alsoKnownAs = "";
	 locationBanda = "";
	 locationCoodernada = "";
	 typeStrand = "";
     Transcripts = "";
     GeneType = "";
     organism = "";
	 sequence = "";
	 GeneType2 = "";
	 name = "";
	 description = ""; 
	 synonyms = "";    


	                if(idEnsembl.length()>0)
        			try
        			{
        				Document document=null;
        				Document document1= null;

        				try {


        					 document = Jsoup.connect("http://www.ensembl.org/Homo_sapiens/Gene/Summary?db=core;g="+idEnsembl).timeout(100*1000).get();
        				}catch(Exception e){

        					System.out.println("Erro no primeiro doc");
        				//	 e.printStackTrace();
        					return false;
        				}
        				try{
        				 document1 =Jsoup.connect("http://www.ensembl.org/Homo_sapiens/Component/Gene/Web/GeneSummary?db=core;g="+idEnsembl).timeout(100*1000).get();
        				}catch(Exception e){
        					System.out.println("Erro no segundo doc");
        					return false;
        				}

        				HtmlParserFooter parserFooter = new HtmlParserFooter(document);
        				HtmlParserFooter parserFooter1 = new HtmlParserFooter(document1);
        				parserFooter.getFooterEnsembl();
        				parserFooter1.getFooterEnsembl1();

        			} catch ( Exception e) {
        				System.out.println("Erro Ensembl");
        				return false;
        				//e.printStackTrace();
        			}



	                if(geneID.length()>0)
					try{

						Document document =  Jsoup.connect("http://www.ncbi.nlm.nih.gov/gene/?term="+geneID).timeout(100*10000).get();
						HtmlParserFooter parserFooter = new HtmlParserFooter(document);
						try{
						parserFooter.getFooterNCBI();
						}
						catch( Exception e){
							System.out.println("Erro no getFooter NCBI");
							return false;
						}


					}
					catch(Exception e ){
						System.out.println("Erro NCBI");
						return false;
					//	e.printStackTrace();
					}
					

     Gene g = new Gene(officialSymbol,geneID,alsoKnownAs,OfficialName,locationBanda,
    		 Transcripts,locationCoodernada,typeStrand,GeneType, idEnsembl,organism,sequence,GeneType2,name,description,synonyms);
     
     System.out.println("officialSymbol -> "+g.getOfficialSymbol());
     System.out.println("OfficialName -> "+g.getOfficialName());
     System.out.println("alsoKnownAs -> "+g.getAlsoKnownAs());
     System.out.println("locationBanda -> "+g.getLocationBanda());
     System.out.println("locationCoodernada -> "+g.getLocationCoodernada());
     System.out.println("typeStrand -> "+g.getTypeStrand());
     System.out.println("Transcripts -> "+g.getTranscripts());
     System.out.println("GeneType -> "+g.getGeneType());
     System.out.println("organism -> "+g.getOrganism());
     System.out.println("sequence -> "+g.getSequence());
     System.out.println("GeneType2 -> "+g.getGeneType2());
     System.out.println("name -> "+g.getName());
     System.out.println("description -> "+g.getDescription());
     System.out.println("sinonyms -> "+g.getSinonyms());
     	     
     
	 listaGenes.addGene(g);


	 return true;
	}

	//  Start get info about Ensembl link 2 and get SINONYMS 
	private void getFooterEnsembl1()
	{
		Elements elements = document.getElementsByClass("row");
		Elements element_rhs = document.getElementsByClass("rhs");
		
		name = element_rhs.eq(0).text();
		System.out.println(" Name <"+name+">");

//		sinonyms = element_rhs.eq(1).text();
//		int fim= sinonyms.indexOf("[");
//		
//		if ( fim != -1 ) {
//			sinonyms = sinonyms.substring(0,fim);
//		} else {
//			sinonyms = "erro ao procurar";
//		}
		
	//	System.out.println(" sinonyms <"+sinonyms+">");
 		
		
		if(testeSumary(elements.eq(4)))
		{
			getFooterSumary(elements.eq(4));
		}
		else
		{
			if(testeSumary(elements.eq(5)))
            {
                getFooterSumary(elements.eq(5));
            }else
             {
				getFooterSumary(elements.eq(3));
             }
		}

	}

	// Ensembl1  -- TEST GENE TYPE
	private Boolean testeSumary(Elements elements)
	{

		Boolean teste =false;
		for(Element element: elements)
		{
			String geneTypeDesc = element.getElementsByClass("lhs").text();
			if(geneTypeDesc.equalsIgnoreCase("Gene type"))
			{
				teste=true;
			}
			else
			{
				teste=false;
			}

		}
		return teste;
	}

// Ensembl1 -- GET GENE TYPE
	private void getFooterSumary(Elements elements)
	{

		for(Element element : elements)
		{
			String nomeDesc = element.getElementsByClass("lhs").text();

				Elements tudo = element.getElementsByClass("rhs");
				for(Element teste: tudo)
				{
					String nomeResult = teste.getElementsByTag("p").text();
					GeneType = nomeResult;
					//System.out.print(" "+nomeDesc+" <"+GeneType+"> \n");
				}
	     }

	}

	// NCBI
	public void getFooterNCBI()
	{
		Elements elements  = null;
		Elements elements1 = null;

		try{
		 elements = document.getElementsByClass("section");
		 elements1 = document.getElementsByClass("gc_col1");
		}catch(Exception e){
			System.out.println(" Erro ao conseguir elementos");
		}

		try{
		getFooterGene1(elements.eq(0));
		}catch(Exception e){
		System.out.println("Erro no metodo FooterGEne1");
		}
		try{
		getFooterGeneContext(elements1.eq(0));
         }catch(Exception e){
		 System.out.println("Erro no metodo footerGeneContext");
		}


	}

	// NCBI LOCATION BANDA AND SEQUENCE
	private void getFooterGeneContext(Elements elements)
	{

		for(Element element: elements)
		{
			int tamanhodt= 0;

		    tamanhodt = element.getElementsByTag("dt").size();

			try{
                for( int i=0 ;i<tamanhodt;i++)
                {
                    if(element.getElementsByClass("mt").get(i).text().equalsIgnoreCase("Location"))
                    {
                        String locationDesc= element.getElementsByTag("dt").get(i).text();
                        String locationResult = element.getElementsByTag("dd").get(i).text();
                           
                            locationBanda =  locationResult;
                            System.out.print("AD "+locationDesc+" "+locationBanda+" \n");
                    }
                    
                    if(element.getElementsByClass("mt").get(i).text().equalsIgnoreCase("Sequence"))
                    {
                        String sequenceDesc = element.getElementsByTag("dt").get(i).text();
                        String sequenceResult = element.getElementsByTag("dd").get(i).text();
                        
                           sequence = sequenceResult;
					       System.out.print("new A "+sequenceDesc+" "+sequenceResult+" \n"); //<----------
                    }
                }
			}catch(Exception e)
			 {
				System.out.println("Nao encontrei as tags");
				return;
			 }
		}
	}

	// NCBI OFFICIAL SYMBOL , GENETYPE, OFFICIALNAME AND ALSOKNOWAS
	private void getFooterGene1(Elements elements)
	{
		for(Element element : elements)
		{
		    int tamanhodt;

            tamanhodt = element.getElementsByTag("dt").size();

			for(int i= 0;i<tamanhodt;i++)
            {
			//String section = element.getElementsByClass("section");
				if(element.getElementsByTag("dt").get(i).text().equalsIgnoreCase("Official Symbol"))
				{
					String officialSymbDesc = element.getElementsByTag("dt").get(i).text();
					String officialSymbResult= element.getElementsByTag("dd").get(i).text();
					
					    officialSymbol = officialSymbResult.replace("provided by HGNC", "");
					    System.out.print("AA "+officialSymbDesc+" "+officialSymbol+" \n");

				}
				if(element.getElementsByTag("dt").get(i).text().equalsIgnoreCase("Official Full Name"))
				{
					String officialNameDesc = element.getElementsByTag("dt").get(i).text();
					String officialNameResult = element.getElementsByTag("dd").get(i).text();
					
				      	OfficialName = officialNameResult.replace("provided by HGNC", "");
					    System.out.print("AB "+officialNameDesc+" "+OfficialName+" \n");
				}
				if(element.getElementsByTag("dt").get(i).text().equalsIgnoreCase("Gene type"))
				{
					String geneTypeDesc = element.getElementsByTag("dt").get(i).text();				
					String geneTypeResult = element.getElementsByTag("dd").get(i).text();
					
					GeneType2 = geneTypeResult;
					System.out.print("AC "+geneTypeDesc+" "+GeneType2+" \n");
				}
				if(element.getElementsByTag("dt").get(i).text().equalsIgnoreCase("Organism"))
				{
					String organismDesc = element.getElementsByTag("dt").get(i).text();
					String organismResult= element.getElementsByTag("dd").get(i).text();
					
					     organism = organismResult;
					     System.out.print("AD "+organismDesc+" "+organism+" \n");
				}
				if(element.getElementsByTag("dt").get(i).text().equalsIgnoreCase("Also known as"))
				{
					String alsoKnownDesc = element.getElementsByTag("dt").get(i).text();
					String alsoKnownResult = element.getElementsByTag("dd").get(i).text();
                           
					       alsoKnownAs = alsoKnownResult;
                           System.out.print("AE "+alsoKnownDesc+" >"+alsoKnownAs+"< \n");
				}
			}
		}

	}

	// <div class="rhs"> Ensemble e sumario
	// <div id="summaryDiv" class="section"> NCBI


	// Ensembl1 -- GET DESCRIPTION,LOCATION AND TRANSCRIPTS  
	private void getFooterEnsembl()
	{
		// mudar corpo dos metodos criados no arquivo! Configura�ao ! Tempplates do Arquivo.

		try{			
		Elements elements_rhs = document.getElementsByClass("rhs");
		description =elements_rhs.eq(0).text();
	//	System.out.println("description >"+description+"<");
		}
		catch(Exception e){ }
		
		Elements elements = document.getElementsByClass("row");

		//get location Codernadas
	 //   System.out.println("try AB1");
		getFooterGeneLS(elements.eq(2));

		this.obterSynonyms(elements.eq( this.getTagIDForText(elements, "Synonyms") ));

		//get Transcripts
	//	System.out.println("try AB2");
		
		getFooterGeneT(elements.eq( this.getTagIDForText(elements, "Transcripts") ));

//		if (testeSumaryTrans(elements.eq(3)))
//        {
//			getFooterGeneT(elements.eq(3));
//		}else
//		 {
//			getFooterGeneT(elements.eq(2));
//		 }


	}
	
	private int getTagIDForText(Elements elements, String txtBusca) {
		int idRetorno = -1;

		int i = 0;
		for (Element element: elements) {
			if ( element.getElementsByClass("lhs").text().equalsIgnoreCase(txtBusca) ) {
				idRetorno = i;
				break;
			}
			i++;
		}

		return idRetorno;
	}
	
	private void obterSynonyms(Elements elements) {

		for(Element element : elements) {

			Elements tagsRHS = element.getElementsByClass("rhs");

			for(Element tag: tagsRHS) {
				synonyms = tag.getElementsByTag("p").text();
			}
		}

	}

    // Ensembl1  -- TEST Transcripts
	private Boolean testeSumaryTrans (Elements elements)
	{

		Boolean teste =false;
		for(Element element: elements)
		{
			String transcriptDesc = element.getElementsByClass("lhs").text();
			if(transcriptDesc.equalsIgnoreCase("Transcripts"))
			{
				teste=true;
			}
			else
			{
				teste=false;
			}

		}
		return teste;
	}


	// Ensembl1 -- LOCATION COORDENADA AND STAND
	private void getFooterGeneLS(Elements elements)
	{
		for(Element element : elements)
		{
			String lhs = element.getElementsByClass("lhs").text();
			Elements tudo = element.getElementsByClass("rhs");

			for(Element teste: tudo){
				
				  String Nome = teste.getElementsByTag("p").text();
			//	  System.out.println(Nome+"\n");

				  String procurada = ": ";
				  int pos = Nome.indexOf(procurada) + procurada.length();
				  int po2 = Nome.length();
				  String n =  Nome.substring(pos,po2);
				  String termino = " ";
				  po2 = n.indexOf(termino);
				  String location = n.substring(0,po2);
				  String strand = n.substring(po2,n.length()-1);

				  locationCoodernada = location;
				  typeStrand  = strand;

			//	System.out.print(" "+lhs+" <"+location+"> \n");
			//	System.out.print("  <"+strand+"> \n");
		    }
	     }
    }

	// Ensembl1 -- Transcripts
	private void getFooterGeneT(Elements elements)
	{
			for(Element element : elements)
			{
				String transcriptsDesc = element.getElementsByClass("lhs").text();
				Elements tudo = element.getElementsByClass("rhs");

				for(Element teste: tudo)
				{
					String numTranscripts = teste.getElementsByTag("p").text();

					  String procurada = "has";
					  String termino = "transcript";
					  int pos = numTranscripts.indexOf(procurada) + procurada.length();
					  int pos1 = numTranscripts.indexOf(termino);
					  String numeroTranscripts =  numTranscripts.substring(pos,pos1);

					  Transcripts = numeroTranscripts;

				//	System.out.print(transcriptsDesc+" <"+Transcripts+"> \n");
			    }
		     }
	}


}
