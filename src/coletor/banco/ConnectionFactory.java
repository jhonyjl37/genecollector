package coletor.banco;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;


public class ConnectionFactory {

		public Connection getConnection() {
	
			 Connection c = null;
			 Statement stmt = null;
			    try {
			      Class.forName("org.sqlite.JDBC");
			      c = DriverManager.getConnection("jdbc:sqlite:test.db");
//			      c = DriverManager.getConnection("jdbc:sqlite:/Users/Filipe/Documents/test.db");
			      System.out.println("Opened database successfully");

			      stmt = c.createStatement();
			      String sql = " CREATE TABLE GENE2 " +			                   
			                   "( gene_id              CHAR(200), " + 
			                   " official_name         CHAR(200), " + 
			                   " also_know_as          CHAR(200), " + 
			                   " location_banda        CHAR(200), " +
			                   " location_coordenada   CHAR(200), " +
			                   " type_strand           CHAR(200), " +
			                   " transcripts           CHAR(200), " +
			                   " gene_type             CHAR(200), " +
			                   " id_ensembl            CHAR(200), " +
			                   " metodo_analise        CHAR(200), " +
			                   " sexo                  CHAR(200), " +
			                   " idade                 CHAR(200), " +
			                   " n_individuos          CHAR(200), " +
			                   " endurance_r_m         CHAR(200), " +
			                   " agudo_cronico         CHAR(200), " +
			                   " up_down               CHAR(200), " +
			                   " official_symbol       CHAR(200),"  +
			                   " referencia            CHAR(200),"
			                   + "organism             CHAR(200),"
			                   + "sequence             CHAR(200),"
			                   + "GeneType2            CHAR(200),"
			                   + "name                 CHAR(200),"
			                   + "description          CHAR(200),"
			                   + "sinonyms             CHAR(200) ); "; 
			      			   
			      try{
			      stmt.executeUpdate(sql);
			      }catch(Exception e){ System.out.println("tabela j� existe!");}
			      			  			      
			      
			      stmt.close();
			      return c;//c.close();
			    } catch ( Exception e ) {
			    	e.printStackTrace();
			      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			      System.exit(0);
			    }
			    System.out.println("Table created successfully");				
			    
			    return null;
		}

	}

 