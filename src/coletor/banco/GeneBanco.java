package coletor.banco;

public class GeneBanco {

	private String geneId;
	private String officialSymbol;
	private String OfficialName;
	private String alsoKnownAs;
	private String References;	
	private String locationBanda;
	private String locationCoodernada;
	private String typeStrand; // forward/reverse Strand;
	private String Transcripts;
	private String GeneType;
	private String idEnsembl;
	private String metodoAnalise;
	private String sexo;
	private String idade;
	private String nIndividuos;
	private String endurance;
	private String AgudoCronico;
	private String upDown;
	//novos atributos adicionados
    private String organism;
    private String sequence;
    private String GeneType2;
    private String name;  
    private String description; 
    private String sinonyms="lucsds";

	public String getOrganism() {
		return organism;
	}

	public void setOrganism(String organism) {
		this.organism = organism;
	}

	public String getSequence() {
		return sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

	public String getGeneType2() {
		return GeneType2;
	}

	public void setGeneType2(String geneType2) {
		GeneType2 = geneType2;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSinonyms() {
		return sinonyms;
	}

	public void setSinonyms(String sinonyms) {
		this.sinonyms = sinonyms;
	}

	public String getOfficialSymbol() {
		return officialSymbol;
	}

	public void setOfficialSymbol(String officialSymbol) {
		this.officialSymbol = officialSymbol;
	}

	public String getGeneId() {
		return geneId;
	}
	
	
	public String getReferences() {
		return References;
	}

	public void setReferences(String references) {
		References = references;
	}

	public void setGeneId(String geneId) {
		this.geneId = geneId;
	}

	public String getOfficialName() {
		return OfficialName;
	}

	public void setOfficialName(String officialName) {
		OfficialName = officialName;
	}

	public String getAlsoKnownAs() {
		return alsoKnownAs;
	}

	public void setAlsoKnownAs(String alsoKnownAs) {
		this.alsoKnownAs = alsoKnownAs;
	}

	public String getLocationBanda() {
		return locationBanda;
	}

	public void setLocationBanda(String locationBanda) {
		this.locationBanda = locationBanda;
	}

	public String getLocationCoodernada() {
		return locationCoodernada;
	}

	public void setLocationCoodernada(String locationCoodernada) {
		this.locationCoodernada = locationCoodernada;
	}

	public String getTypeStrand() {
		return typeStrand;
	}

	public void setTypeStrand(String typeStrand) {
		this.typeStrand = typeStrand;
	}

	public String getTranscripts() {
		return Transcripts;
	}

	public void setTranscripts(String transcripts) {
		Transcripts = transcripts;
	}

	public String getGeneType() {
		return GeneType;
	}

	public void setGeneType(String geneType) {
		GeneType = geneType;
	}

	public String getIdEnsembl() {
		return idEnsembl;
	}

	public void setIdEnsembl(String idEnsembl) {
		this.idEnsembl = idEnsembl;
	}

	public String getMetodoAnalise() {
		return metodoAnalise;
	}

	public void setMetodoAnalise(String metodoAnalise) {
		this.metodoAnalise = metodoAnalise;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getIdade() {
		return idade;
	}

	public void setIdade(String idade) {
		this.idade = idade;
	}

	public String getnIndividuos() {
		return nIndividuos;
	}

	public void setnIndividuos(String nIndividuos) {
		this.nIndividuos = nIndividuos;
	}

	public String getEndurance() {
		return endurance;
	}

	public void setEndurance(String endurance) {
		this.endurance = endurance;
	}

	public String getAgudoCronico() {
		return AgudoCronico;
	}

	public void setAgudoCronico(String agudoCronico) {
		AgudoCronico = agudoCronico;
	}

	public String getUpDown() {
		return upDown;
	}

	public void setUpDown(String upDown) {
		this.upDown = upDown;
	}

}
