package coletor.banco;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class GeneDao {
	
	private Connection c;
	
	public GeneDao() {
		c = new ConnectionFactory().getConnection();
	}

	public GeneBanco getGenePorId(String id,boolean b ) {
		try {
			GeneBanco gene = new GeneBanco();
			
			 
		     Statement stmt = null;					  		    		     
		     stmt = c.createStatement();
		     ResultSet rs;
		     
			if ( b == true){ // caso b seja true ele vai assumir que o id � do tipo NCBI, caso false Ensembl
				 rs = stmt.executeQuery("SELECT* FROM GENE2 WHERE" + " gene_id = '"
						+ id+"' ;");
					
			}else{
				 rs = stmt.executeQuery("SELECT* FROM GENE2 WHERE" + " id_ensembl = '"
								+ id +"' ;");
			}
			

			while (rs.next()) {
				gene.setGeneId(rs.getString("gene_id"));
				gene.setOfficialName(rs.getString("official_name"));
				gene.setAlsoKnownAs(rs.getString("also_know_as"));
				gene.setLocationBanda(rs.getString("location_banda"));
				gene.setLocationCoodernada(rs.getString("location_coordenada"));
				gene.setTypeStrand(rs.getString("type_strand"));
				gene.setTranscripts(rs.getString("transcripts"));
				gene.setGeneType(rs.getString("gene_type"));
				gene.setIdEnsembl(rs.getString("id_ensembl"));
				gene.setMetodoAnalise(rs.getString("metodo_analise"));
				gene.setSexo(rs.getString("sexo"));
				gene.setIdade(rs.getString("idade"));
				gene.setnIndividuos(rs.getString("n_individuos"));
				gene.setEndurance(rs.getString("endurance_r_m"));
				gene.setAgudoCronico(rs.getString("agudo_cronico"));
				gene.setUpDown(rs.getString("up_down"));
				gene.setOfficialSymbol(rs.getString("official_symbol"));
				gene.setReferences(rs.getString("referencia"));
				gene.setOrganism(rs.getString("organism"));
				gene.setSequence(rs.getString("sequence"));
				gene.setGeneType2(rs.getString("GeneType2"));
				gene.setName(rs.getString("name"));
				gene.setDescription(rs.getString("description"));
				gene.setSinonyms(rs.getString("sinonyms"));			

			}

			      rs.close();
			      stmt.close();
			     
			return gene;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<GeneBanco> getListaGeneBanco() {
		try {
			
			 
		    Statement stmt = null;
		    List<GeneBanco> genes = new ArrayList<GeneBanco>();
					     								
		      stmt = c.createStatement();
		      ResultSet rs = stmt.executeQuery( "SELECT * FROM GENE2;" );
			// rs guarda uma lista de genes

			while (rs.next()) {
				// criando o objeto geneBanco
				GeneBanco gene = new GeneBanco();

				gene.setGeneId(rs.getString("gene_id"));
				gene.setOfficialName(rs.getString("official_name"));
				gene.setAlsoKnownAs(rs.getString("also_know_as"));
				gene.setLocationBanda(rs.getString("location_banda"));
				gene.setLocationCoodernada(rs.getString("location_coordenada"));
				gene.setTypeStrand(rs.getString("type_strand"));
				gene.setTranscripts(rs.getString("transcripts"));
				gene.setGeneType(rs.getString("gene_type"));
				gene.setIdEnsembl(rs.getString("id_ensembl"));
				gene.setMetodoAnalise(rs.getString("metodo_analise"));
				gene.setSexo(rs.getString("sexo"));
				gene.setIdade(rs.getString("idade"));
				gene.setnIndividuos(rs.getString("n_individuos"));
				gene.setEndurance(rs.getString("endurance_r_m"));
				gene.setAgudoCronico(rs.getString("agudo_cronico"));
				gene.setUpDown(rs.getString("up_down"));
				gene.setOfficialSymbol(rs.getString("official_symbol"));
				gene.setReferences(rs.getString("referencia"));
				gene.setOrganism(rs.getString("organism"));
				gene.setSequence(rs.getString("sequence"));
				gene.setGeneType2(rs.getString("GeneType2"));
				gene.setName(rs.getString("name"));
				gene.setDescription(rs.getString("description"));
				gene.setSinonyms(rs.getString("sinonyms"));		
				// adicionando o objeto � lista
				genes.add(gene);
			}

			rs.close();
			stmt.close();
			return genes;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void adicionarGene(GeneBanco gene) {

	    
	    Statement stmt = null;
	    
		// preparar a sql
		String sql = "INSERT INTO GENE2(gene_id, official_name, also_know_as,"
				+ "location_banda, location_coordenada, type_strand, transcripts,"
				+ "gene_type, id_ensembl, metodo_analise, sexo, idade, n_individuos, "
				+ "endurance_r_m, agudo_cronico, up_down, official_symbol, referencia,"
				+ "organism, sequence, GeneType2, name, description, sinonyms) values ('";
		try {			

		      stmt = c.createStatement();
						
			// seta os valores

			sql+=gene.getGeneId()+"','";
			sql+=gene.getOfficialName()+"','";
			sql+=gene.getAlsoKnownAs()+"','";
			sql+=gene.getLocationBanda()+"','";
			sql+= gene.getLocationCoodernada()+"','";
			sql+=gene.getTypeStrand()+"','";
			sql+= gene.getTranscripts()+"','";
			sql+=gene.getGeneType()+"','";
			sql+= gene.getIdEnsembl()+"','";
			sql+= gene.getMetodoAnalise()+"','";
			sql+= gene.getSexo()+"','";
			sql+= gene.getIdade()+"','";
			sql+= gene.getnIndividuos()+"','";
			sql+= gene.getEndurance()+"','";
			sql+= gene.getAgudoCronico()+"','";
			sql+= gene.getUpDown()+"','";
			sql+= gene.getOfficialSymbol()+"','";
			sql+= gene.getReferences()+"','";
			sql+= gene.getOrganism()+"','";
			sql+= gene.getSequence()+"','";
			sql+= gene.getGeneType2()+"','";
			sql+= gene.getName()+"','";
			sql+= gene.getDescription()+"','";
			sql+= gene.getSinonyms()+"'); ";
									

			// executa
		      stmt.executeUpdate(sql);		    
		      stmt.close();
		      //c.commit();
		     
		    
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}

	public void alterarGene(GeneBanco gen) {
		
		   
		    Statement stmt = null;
		    
		String sql = "update GENE2 set official_name = '"+gen.getOfficialName()+"', also_know_as = '"+gen.getAlsoKnownAs()+"', "
				+ "location_banda = '"+gen.getLocationBanda()+"', location_coordenada = '"+gen.getLocationCoodernada()+"', "
				+ "type_strand = '"+gen.getTypeStrand()+"', transcripts = '"+gen.getTranscripts()+"', "
				+ "gene_type = '"+ gen.getGeneType()+"', id_ensembl = '"+gen.getIdEnsembl()+"', metodo_analise = '"+gen.getMetodoAnalise()+"', sexo = '"+gen.getSexo()+"', idade = '"+gen.getIdade()+"', n_individuos = '"+gen.getnIndividuos()+"', "
				+ "endurance_r_m = '"+gen.getEndurance()+"', agudo_cronico = '"+gen.getAgudoCronico()+"', up_down = '"+
				gen.getUpDown()+"', official_symbol = '"+gen.getOfficialSymbol()+"', referencia = '"+gen.getReferences()
				+"', organism = '"+gen.getOrganism()
				+"', sequence = '"+gen.getSequence()
				+"', GeneType2 = '"+gen.getGeneType2()
				+"', name = '"+gen.getName()
				+"', description = '"+gen.getDescription()
				+"', sinonyms = '"+gen.getSinonyms()+"' where  gene_id = '"+ gen.getGeneId()+"' ;";

			
		try {			     			
			      stmt = c.createStatement();

			      stmt.executeUpdate(sql);
			    //  c.commit();
			      stmt.close();
			      
		} catch (Exception e) {
		e.printStackTrace();
		}

	}

	public void removerGene(GeneBanco gen) {
		
	    Statement stmt = null;
		
		try {
								    
		      stmt = c.createStatement();
		      String sql = "DELETE FROM GENE2 WHERE gene_id = '"+gen.getGeneId()+"';";
		      stmt.executeUpdate(sql);
		    //  c.commit();
		      stmt.close();
		      
		 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
}
