package coletor.telas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.TitledBorder;
import javax.swing.JTextPane;
import javax.swing.UIManager;
import org.eclipse.wb.swing.FocusTraversalOnArray;
import java.awt.Component;
import java.awt.Color;

public class SoftwareInfo extends JFrame {

	private JPanel contentPane;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SoftwareInfo frame = new SoftwareInfo();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SoftwareInfo() {
		setTitle("Informa\u00E7\u00F5es do Software");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 588, 457);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Gene Collector 1.1");
		lblNewLabel.setBounds(10, 5, 551, 25);
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
		contentPane.add(lblNewLabel);
		
		JPanel panel = new JPanel();
		panel.setFont(new Font("Tahoma", Font.BOLD, 11));
		panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Lista de autores, desenvolvedores e colaboradores:", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(10, 192, 551, 225);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 15, 526, 200);
		panel.add(scrollPane);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{"Christina Pacheco Santos Martin", "UECE", "christinaosvaldo@yahoo.com.br"},
				{"V\u00E2nia Marilande Ceccatto", "UECE", "vceccatto@yahoo.com.br"},
				{"Samuel Gon\u00E7alves Lopes", "UFERSA", "samuca.lopes@hotmail.com"},
				{"Willyane de Paiva Silva", "UFERSA", "willyane_rn@hotmail.com"},
				{"Jhony Lucas Cavalcante da Silva", "UFERSA", "jhonyjl@gmail.com"},
				{"Renato Pedrosa Vasconcelos", "UFERSA", "renato.pedrosa13@hotmail.com"},
				{"Antonio Almir Araujo Neto", "UFERSA", "almirneto87@hotmail.com"},
				{"Stela Mirla da Silva Felipe", "UECE", "stelamirla@gmail.com"},
				{"Milca Magalh\u00E3es Dias de Carvalho Soares", "UECE", "milcamagal@gmail.com"},
				{"Juliana Os\u00F3rio Alves", "UECE", "juosorio@gmail.com"},
				{"Denise Pires de Carvalho", "UFRJ", "dencarv@gmail.com"},
			},
			new String[] {
				"Nome", "Institui\u00E7\u00E3o", "E-mail"
			}
		) {
			boolean[] columnEditables = new boolean[] {
				false, false, false
			};
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		panel.setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{scrollPane, table}));
		
		JLabel lblNewLabel_1 = new JLabel("<html><p align=\"justify\"><b>Gene Collector</b> foi concebido para o uso por pesquisadores e estudantes das Ci\u00EAncias da Vida, para que possam coletar informa\u00E7\u00F5es dispon\u00EDveis em dois dos principais bancos de dados biol\u00F3gicos: <i>NCBI Gene </i>(http://www.ncbi.nlm.nih.gov/gene/) e <i>Ensembl Genome Browser</i> (http://www.ensembl.org/), com maior facilidade e rapidez. Com o programa Gene Collector o usu\u00E1rio pode reunir diversas informa\u00E7\u00F5es sobre um conjunto de genes, como localiza\u00E7\u00E3o, s\u00EDmbolo oficial, sin\u00F4nimos, nome oficial e tipo de produto g\u00EAnico, com a op\u00E7\u00E3o de buscar os dados nos bancos de dados <i>NCBI Gene</i>, <i>Ensembl</i> ou ambos. Uma s\u00EDntese de alguns dados obtidos tamb\u00E9m \u00E9 produzida para fornecer uma vis\u00E3o geral do conjunto de genes avaliado.</p></html>");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.LEFT);
		lblNewLabel_1.setBounds(15, 25, 546, 159);
		contentPane.add(lblNewLabel_1);
		table.getColumnModel().getColumn(0).setResizable(false);
		table.getColumnModel().getColumn(0).setPreferredWidth(210);
		table.getColumnModel().getColumn(1).setResizable(false);
		table.getColumnModel().getColumn(1).setPreferredWidth(61);
		table.getColumnModel().getColumn(2).setResizable(false);
		table.getColumnModel().getColumn(2).setPreferredWidth(160);
	}
}
