package coletor.telas;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.UIManager;

import org.eclipse.wb.swing.FocusTraversalOnArray;

import coletor.com.IdGene;
import coletor.com.Principal;
import coletor.tables.GeneTableModel;

import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;

import java.awt.Dimension;

import javax.swing.LayoutStyle.ComponentPlacement;

public class AddGenes extends JFrame {

	private GeneTableModel model;	
	private Thread thr1;
	private static Idioma_Language idioma = FrameWork.getIdioma();
	
	// Elementos gr�ficos do JFrame
	private JPanel contentPane;	
	private JLabel textIdEntrez;
	private JLabel textIDEnsembl;
	private JTextField textNCBI;
	private JTextField textEnsembl;
	private JButton buttonAdicionar;
	private JScrollPane scrollConjuntoGenes;
	private static JTable tableListaColeta;
	private JButton buttonBaixarDados;
	private JButton buttonCancelar;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddGenes frame = new AddGenes();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AddGenes() {
		setResizable(false);
		 
		
		// Criando os modelos das tabelas
		
		model  = new GeneTableModel(FrameWork.getListaBaixar(),FrameWork.getIdioma());
		
		
		
		setTitle(idioma.getTitleAddGenes());
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 461, 643);
		contentPane = new JPanel();
		contentPane.setMaximumSize(new Dimension(10, 32767));
		contentPane.setToolTipText("");
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		
		scrollConjuntoGenes = new JScrollPane();
		
		tableListaColeta = new JTable();
		tableListaColeta.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				switch (e.getKeyCode()){
				 case KeyEvent.VK_DELETE:				 	 
					 ((GeneTableModel)tableListaColeta.getModel()).excluir(tableListaColeta.getSelectedRow());
				 	 break;
				 default:
					 break;
			 }
			}
		});
		tableListaColeta.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "", 
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		tableListaColeta.setModel(model);
		
		
		tableListaColeta.setFont(new Font("Tahoma", Font.PLAIN, 13));	
		tableListaColeta.setBackground(new Color(248, 248, 255));
		tableListaColeta.setRowHeight(23);
		tableListaColeta.getTableHeader().setReorderingAllowed(false);
		
		
		tableListaColeta.setShowGrid(false);
		tableListaColeta.setShowVerticalLines(false);
		tableListaColeta.setShowHorizontalLines(false);
		
		tableListaColeta.setDefaultRenderer(Object.class, new DefaultTableCellRenderer() {  
		    public Component getTableCellRendererComponent(JTable table, Object value,boolean isSelected, boolean hasFocus,
		    		int row, int column) {  
		    	
			       super.getTableCellRendererComponent(table, value, isSelected,hasFocus, row, column);  
			      
			       
			       
			        if (row % 2 == 0) {  
			            setBackground(new Color(245,245,249));  
			        } else {  
			        	 setBackground(new Color(237,237,244)); //
			        }  
			         
			        
			        if (isSelected == true){
			        	
			        	 setForeground(new Color(255,255,255));
			        	  setBackground(new Color(0,180,255)); 
			        	
			        }else{
			        	setForeground(new Color(46,34,36));
			        }
			        
			        return this; 
			        
			    }  
			}); 
		
		scrollConjuntoGenes.setViewportView(tableListaColeta);
		
		textIdEntrez = new JLabel(idioma.getTextIdEntrez());
		
		textIDEnsembl = new JLabel(idioma.getTextIdEnsembl());
		
		textNCBI = new JTextField();
		textNCBI.setColumns(10);
		
		textEnsembl = new JTextField();
		textEnsembl.setColumns(10);
		
		buttonAdicionar = new JButton(idioma.getButtonAdicionar());
		buttonAdicionar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				IdGene  g = new IdGene(textNCBI.getText(),textEnsembl.getText());				
				model.inserir(g);
				textNCBI.setText("");
				textEnsembl.setText("");
			}
		});
		
		buttonBaixarDados = new JButton(idioma.getButtonBaixarDados());
		buttonBaixarDados.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if ( buttonBaixarDados.isEnabled() == true){
					//Criando um Thread Para auxiliar a coletar das informa��es dos genes na internet..
					buttonBaixarDados.setEnabled(false);
			          Runnable r = new Runnable() {
					  public void run() {
							FrameWork.iniciarColeta = new Principal(model.ListGeneID(),buttonBaixarDados, buttonCancelar);
							FrameWork.iniciarColeta.run(model);
					  }
					};
					
					thr1 = new Thread(r);
					thr1.start();
					buttonCancelar.setEnabled(true);
				}
				
			}
		});
		
		
		buttonCancelar = new JButton(idioma.getButtonCancelar());
		buttonCancelar.setEnabled(false);
		buttonCancelar.addActionListener(new ActionListener() {
			@SuppressWarnings({ "unchecked", "deprecation" })
			public void actionPerformed(ActionEvent arg0) {
				thr1.stop();
				buttonBaixarDados.setEnabled(true);
				buttonCancelar.setEnabled(false);
				 
				for(IdGene g : (ArrayList<IdGene>) model.ListGeneID()){ 
					if (g.getWhatdo().equals(getIdioma().getStatus1())) {
					
					g.setWhatdo("");
						model.fireTableDataChanged();
					}
				}
			}
		});
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(10)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(scrollConjuntoGenes, GroupLayout.DEFAULT_SIZE, 425, Short.MAX_VALUE)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(textIdEntrez, GroupLayout.PREFERRED_SIZE, 86, GroupLayout.PREFERRED_SIZE)
							.addGap(10)
							.addComponent(textIDEnsembl, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(textNCBI, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(10)
							.addComponent(textEnsembl, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED, 99, Short.MAX_VALUE)
							.addComponent(buttonAdicionar, GroupLayout.PREFERRED_SIZE, 89, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(buttonBaixarDados, GroupLayout.PREFERRED_SIZE, 200, GroupLayout.PREFERRED_SIZE)
							.addGap(14)
							.addComponent(buttonCancelar, GroupLayout.PREFERRED_SIZE, 200, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(6)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(textIdEntrez)
						.addComponent(textIDEnsembl))
					.addGap(4)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(1)
							.addComponent(textNCBI, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(1)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(textEnsembl, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(buttonAdicionar))))
					.addGap(9)
					.addComponent(scrollConjuntoGenes, GroupLayout.DEFAULT_SIZE, 510, Short.MAX_VALUE)
					.addGap(11)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(buttonBaixarDados)
						.addComponent(buttonCancelar))
					.addGap(6))
		);
		contentPane.setLayout(gl_contentPane);
		contentPane.setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{
				scrollConjuntoGenes, tableListaColeta, textIdEntrez, textIDEnsembl, textNCBI, 
				textEnsembl, buttonAdicionar, buttonBaixarDados, buttonCancelar}));
	}

	/**
	 * @return the idioma
	 */
	public static Idioma_Language getIdioma() {
		return idioma;
	}

	/**
	 * @param idioma the idioma to set
	 */
	public static void setIdioma(Idioma_Language idioma) {
		AddGenes.idioma = idioma;
	}
}
