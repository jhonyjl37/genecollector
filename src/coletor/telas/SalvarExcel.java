package coletor.telas;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.filechooser.FileNameExtensionFilter;

import coletor.com.Gene;







import java.io.File;
import java.io.IOException;
import java.util.Locale;
 





import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.VerticalAlignment;
import jxl.write.Alignment;
import jxl.write.Formula;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

public class SalvarExcel extends JFrame {
	public SalvarExcel() {
	}

	private WritableCellFormat timesBoldUnderline;
	private WritableCellFormat times;
	private String inputArquivo;
	private Idioma_Language idioma;
	private WritableSheet sheet;
	
	//Lista de Genes
	ArrayList<Gene> listaGenes = FrameWork.getListaPE() ;
	
	public void setOutputFile(String inputArquivo) {
		this.inputArquivo = inputArquivo;
	}
	
	public void insere() throws IOException, WriteException {
		// Cria um novo arquivo
		File arquivo = new File(inputArquivo);
		WorkbookSettings wbSettings = new WorkbookSettings();
		 
		wbSettings.setLocale(new Locale("pt", "BR"));
		 
		WritableWorkbook workbook = Workbook.createWorkbook(arquivo, wbSettings);
		// Define um nome para a planilha
		workbook.createSheet("Jexcel", 0);
		sheet = workbook.getSheet(0);
		criaLabel(sheet);
		defineConteudo(sheet);
		 
		workbook.write();
		workbook.close();
	}
	
	// M�todo respons�vel pela defini��o das labels
	private void criaLabel(WritableSheet sheet)	throws WriteException {
		// Cria o tipo de fonte como TIMES e tamanho
		WritableFont times10pt = new WritableFont(WritableFont.ARIAL, 10);
		 
		// Define o formato da c�lula
		times = new WritableCellFormat(times10pt);
		
		// Efetua a quebra autom�tica das c�lulas
		times.setWrap(true);
		 
		// Cria a fonte em negrito com underlines
		WritableFont times10ptBoldUnderline = new WritableFont(
		WritableFont.ARIAL, 10, WritableFont.BOLD, false);
		//UnderlineStyle.SINGLE);
		timesBoldUnderline = new WritableCellFormat(times10ptBoldUnderline);
		
		 
		timesBoldUnderline.setAlignment(Alignment.CENTRE);
		// Efetua a quebra autom�tica das c�lulas
		timesBoldUnderline.setWrap(true);
		 
		CellView cv = new CellView();
		cv.setFormat(times); //Bom pessoal, � isso ai, qualquer d�vida � s� avisar.
		cv.setFormat(timesBoldUnderline);
		cv.setAutosize(true);
	
		idioma = new Idioma_Language();
		
	}
	 
	private void defineConteudo(WritableSheet sheet) 
			throws WriteException,RowsExceededException {
			 		
	   for(int i= 0; i<listaGenes.size();i++)  {
		  
		   addLabel(sheet, 0, i+1, listaGenes.get(i).getOfficialSymbol() );
		   addLabel(sheet, 1, i+1, listaGenes.get(i).getGeneId() );
		   addLabel(sheet, 2, i+1, listaGenes.get(i).getAlsoKnownAs());
		   addLabel(sheet, 3, i+1, listaGenes.get(i).getOfficialName());
		   addLabel(sheet, 4, i+1, listaGenes.get(i).getLocationBanda());
		   addLabel(sheet, 5, i+1, "");
		   addLabel(sheet, 6, i+1, listaGenes.get(i).getTranscripts());
		   addLabel(sheet, 7, i+1, listaGenes.get(i).getLocationCoodernada());
		   addLabel(sheet, 8, i+1, listaGenes.get(i).getTypeStrand());
		   addLabel(sheet, 9, i+1, listaGenes.get(i).getGeneType());
		   addLabel(sheet, 10, i+1, listaGenes.get(i).getIdEnsembl());
		   
	   }   
	}
	 
	// Adiciona cabecalho
	private void addCaption(WritableSheet planilha, int coluna, int linha, String s) throws RowsExceededException, 
	WriteException {
	    Label label;
	    label = new Label(coluna, linha, s, timesBoldUnderline);
	    planilha.addCell(label);
	}
	 
	private void addLabel(WritableSheet planilha, int coluna, int linha, String s) throws WriteException, 
	RowsExceededException {
	   Label label;
	   label = new Label(coluna, linha, s, times);
	   planilha.addCell(label);
	}
	
	/*
	 * Adicionar um novo gene a lista.
	 */
	public void addGene(Gene g){
		
		listaGenes.add(g);
		FrameWork.passarListaGene(listaGenes);
		
	}
	
	/*
	 * Apagando todos os genes que est�o na lista.
	 */
	public void clearGenes(){
		listaGenes.clear();
	}
	
	/*
	 * Salvar a lista de genes para um formato do Excel.
	 */	
	public boolean salvarExcel() throws IOException{
		   String path = "";
		   
		   if(listaGenes.size()== 0){
			   
			   //Lista de genes vazia;
			   return false;
		   }
		   
		      //conseguindo o endere�o onde o arquivo ser� salvo.
              path= getPath("xls");
              
              //Criando e salvando arquivo do Excel.
              setOutputFile(path);
              try {
              insere();
              return true;
              } catch (WriteException e) {
              // TODO Auto-generated catch block
              e.printStackTrace();
              } catch (IOException e) {
              // TODO Auto-generated catch block
              e.printStackTrace();
              }
              
              
        return false;
	}
	
	public static String getPath(String ext){
		 //Alterando o skin da janela save.
		 try {	
	 		   
	 		   UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());                  
        } catch (Exception e) {
            e.printStackTrace();
        }
		 
		 		 
		 // Mostrando para o usuario a janela save, para o mesmo esolher o local de destino do arquivo.
		
		    JFileChooser chooser = new JFileChooser ();
		    chooser.setFileSelectionMode(JFileChooser.CUSTOM_DIALOG);  
	 		FileNameExtensionFilter filter = new FileNameExtensionFilter ("*."+ext,ext);  
	 		chooser.addChoosableFileFilter(filter);
	 		chooser.setFileFilter(filter);
	 		chooser.setAcceptAllFileFilterUsed(false); 
	        
	 		  int op = chooser.showSaveDialog(chooser);  
             if(op == JFileChooser.APPROVE_OPTION){  
                 File arq = chooser.getSelectedFile();  
                  String path = arq.toString()+"."+ext;  
                  System.out.println(path);
                  return path;
                                   
             }else{
           	  //Falha ao conseguir o caminho do novo aquivo;
           	  return "";
             }
             
	}
	
	public void configurarPlanilhaExcel(){
		// Escreve os cabe�alhos
		 
		sheet.setColumnView(0, 25);
		sheet.setColumnView(1, 25);
		sheet.setColumnView(2, 25);
		sheet.setColumnView(3, 50);
		sheet.setColumnView(4, 25);
		sheet.setColumnView(5, 25);
		sheet.setColumnView(6, 25);
		sheet.setColumnView(7, 25);
		sheet.setColumnView(8, 25);
		sheet.setColumnView(9, 25);
		sheet.setColumnView(10, 25);
		sheet.setColumnView(11, 25);
		sheet.setColumnView(12, 25);
		sheet.setColumnView(13, 25);
		sheet.setColumnView(14, 25);
		sheet.setColumnView(15, 25);
		sheet.setColumnView(16, 25);
		sheet.setColumnView(17, 25);
	
		try {
			addCaption(sheet, 0, 0, idioma.getTableResultados_1());
			addCaption(sheet, 1, 0, idioma.getTableResultados_2());
			addCaption(sheet, 2, 0, idioma.getTableResultados_3());
			addCaption(sheet, 3, 0, idioma.getTableResultados_4());
			addCaption(sheet, 4, 0, idioma.getTableResultados_5());
			addCaption(sheet, 5, 0, idioma.getTableResultados_6());
			addCaption(sheet, 6, 0, idioma.getTableResultados_7());
			addCaption(sheet, 7, 0, idioma.getTableResultados_8());
			addCaption(sheet, 8, 0, idioma.getTableResultados_9());
			addCaption(sheet, 9, 0, idioma.getTableResultados_10());
			addCaption(sheet, 10, 0, idioma.getTableResultados_11());
			addCaption(sheet, 11, 0, idioma.getTableResultados_12());
			addCaption(sheet, 12, 0, idioma.getTableResultados_13());
			addCaption(sheet, 13, 0, idioma.getTableResultados_14());
			addCaption(sheet, 14, 0, idioma.getTableResultados_15());
			addCaption(sheet, 15, 0, idioma.getTableResultados_16());
			addCaption(sheet, 16, 0, idioma.getTableResultados_17());
			addCaption(sheet, 17, 0, idioma.getTableResultados_18());

		} catch (RowsExceededException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (WriteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	
}
