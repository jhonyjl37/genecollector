package coletor.telas;

// importanto todos os m�todos est�ticos de SalvarExcel
import static coletor.telas.SalvarExcel.getPath;
import static coletor.com.TextGenerator.*;
import static coletor.ideograma.IdeogramaGenerator.*;

import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableCellRenderer;

import com.sun.org.apache.xml.internal.resolver.helpers.FileURL;

import coletor.banco.GeneBanco;
import coletor.banco.GeneDao;
import coletor.com.CarregarXls;
import coletor.com.Gene;
import coletor.com.IdGene;
import coletor.com.PlanilhaBanco;
import coletor.com.Principal;
import coletor.tables.ExcelTableModel;
import coletor.tables.GeneBancoTableModel;
import coletor.tables.GeneTableModel;

public class FrameWork extends JFrame {

	private JPanel contentPane;
	private static ExcelTableModel model2;
	private GeneBancoTableModel model3;

	private Thread thr1;
	private PlanilhaBanco p;

	// Listas mais importante desse JPanel..
	static ArrayList<IdGene> listaBaixar = new ArrayList(); // lista de Genes
															// apenas com
															// informa��es de ID
	static ArrayList<Gene> listaPE = new ArrayList(); // lista de GEnes completa
														// de informa��o..

	public static ArrayList<IdGene> getListaBaixar() {
		return listaBaixar;
	}

	public static void setListaBaixar(ArrayList<IdGene> listaBaixar) {
		FrameWork.listaBaixar = listaBaixar;
	}

	private ArrayList<GeneBanco> listaBanco; // lista de Genes utilizado no
												// banco de dados..

	static Principal iniciarColeta = null;
	private static JTable tableGenesBaixados;
	private JPanel panelBD;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTable tableBanco;
	private GeneDao bancoDeDados;
	private SoftwareInfo softwareInfo = new SoftwareInfo();; // Frame de
																// informa��o

	private boolean bancoConecao;

	private static Idioma_Language idioma = new Idioma_Language();

	private JButton btnSalvarAltera��es;
	private JButton button;
	private JButton bttnGerarPlanilha;
	private JButton btnSalvarDados;
	private JButton btnInserirId;
	private JButton btnFornecerPlanilha;
	private JButton btnBaixarTutorial;
	private JButton btnPesquisa;
	private JButton btnGerarPlanilha;

	private JPanel panelColetaDados;
	private JPanel panelPesquisa;

	private JScrollPane scrollBDLocal;
	private JScrollPane scrollResultados;

	private JLabel textIdEntrez;
	private JLabel textIdEnsembl;

	private JOptionPane op = new JOptionPane();
	private JOptionPane pane;
	private final ButtonGroup buttonGroup = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {

					// Executando os dois arquivos bat necess�rios para iniciar
					// o mysql e o apache

					// String command = "cmd /c \"C:\\xampp\\mysql_start.bat\"";
					// Process process = Runtime.getRuntime().exec(command);

					FrameWork frame = new FrameWork();
					frame.setLocationRelativeTo(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public static ArrayList<Gene> getListaPE() {
		return listaPE;
	}

	public static void setListaPE(ArrayList<Gene> genes) {
		listaPE = genes;
	}

	/**
	 * @return the idioma
	 */
	public static Idioma_Language getIdioma() {
		return idioma;
	}

	/**
	 * @param idioma
	 *            the idioma to set
	 */
	public static void setIdioma(Idioma_Language idioma) {
		FrameWork.idioma = idioma;
	}

	/**
	 * Create the frame.
	 */

	public FrameWork() {
		setTitle("Gene Collector 1.1");

		setIconImage(Toolkit.getDefaultToolkit().getImage(
				FrameWork.class.getResource("/busca/Gc.ico")));
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {

				try {
					// parando os dois arquivos bat necess�rios para iniciar o
					// mysql e o apache
					String command = "cmd /c \"C:\\xampp\\mysql_start.bat\"";
					Process process = Runtime.getRuntime().exec(command);

				} catch (Exception e) {

				}

				System.exit(0);
			}
		});

		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 944, 631);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setBackground(new Color(244, 244, 244));

		/*
		 * Alterando o Skin do programa para ser o mesmo do sistema utilizado
		 * pelo us�ario.
		 */
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Criando a conex�o com o banco de dados

		try {
			bancoDeDados = new GeneDao();
			System.out.println("Sucesso com GEneDao");
			// Pegando a lista do banco de dados
			listaBanco = (ArrayList<GeneBanco>) bancoDeDados
					.getListaGeneBanco();
			bancoConecao = true;
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,
					idioma.getjOptionErroConex�oBD(),
					idioma.getjOptionErroTitulo(), JOptionPane.ERROR_MESSAGE);
			bancoConecao = false;
		}

		new GeneTableModel(getIdioma());
		model2 = new ExcelTableModel(getIdioma());

		if (bancoConecao == true && listaBanco.size() > 0) {

			for (GeneBanco g : listaBanco) {
				System.out.println(g.getGeneId());
			}

			model3 = new GeneBancoTableModel(listaBanco, getIdioma());
		} else {
			model3 = new GeneBancoTableModel(getIdioma());
		}

		final JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);

		// if (bancoConecao== false)
		// btnSalvarDadosNo.setEnabled(false);

		JLabel lblGeneCollector = new JLabel("Gene Collector");
		lblGeneCollector.setFont(new Font("Tahoma", Font.BOLD, 18));

		button = new JButton("");
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				softwareInfo.setVisible(true);
			}
		});
		button.setIcon(new ImageIcon(FrameWork.class
				.getResource("/busca/info.png")));

		JLabel lblIdiomalanguage = new JLabel("Idioma/Language:");

		final JComboBox comboIdioma = new JComboBox();
		comboIdioma.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (comboIdioma.getSelectedIndex() == 0)
					getIdioma().traduzir();
				else
					getIdioma().translate();

				// Elementos do panelColetaGenes
				tabbedPane.setTitleAt(0, idioma.getPanelColetaDados());
				btnInserirId.setText(idioma.getBtnInserirId());
				bttnGerarPlanilha.setText(idioma.getBttnGerarPlanilha());
				btnSalvarDados.setText(idioma.getBtnSalvarDados());

				// Elementos do panelBD
				tabbedPane.setTitleAt(1, idioma.getPanelBD());
				panelPesquisa.setBorder(new TitledBorder(new TitledBorder(
						UIManager.getBorder("TitledBorder.border"), "",
						TitledBorder.LEADING, TitledBorder.TOP, null, null),
						idioma.getPanelPesquisa(), TitledBorder.LEADING,
						TitledBorder.TOP, null, null));
				scrollBDLocal.setBorder(new TitledBorder(UIManager
						.getBorder("TitledBorder.border"), idioma
						.getScrollBDLocal(), TitledBorder.LEADING,
						TitledBorder.TOP, null, null));
				textIdEntrez.setText(idioma.getTextIdEntrez());
				textIdEnsembl.setText(idioma.getTextIdEnsembl());
				btnSalvarAltera��es.setText(idioma.getBtnSalvarAltera��es());
				btnGerarPlanilha.setText(idioma.getBtnGerarPlanilha());

				scrollResultados.setBorder(new TitledBorder(UIManager
						.getBorder("TitledBorder.border"), idioma
						.getScrollResultados(), TitledBorder.LEADING,
						TitledBorder.TOP, null, null));

				btnFornecerPlanilha.setText(idioma.getBtnFornecerPlanilha());
				btnBaixarTutorial.setText(idioma.getBtnBaixarTutorial());

				model2.setIdioma(idioma);
				model2.fireTableStructureChanged();
				model3.setIdioma(idioma);
				model3.fireTableStructureChanged();

				AddGenes.setIdioma(idioma);

				// i.getTxtpnCriadoPelaEmpressa().setText(idioma.getInforma��o());
				// i.getLblMes().setText(idioma.getM�s());

				setColumnSize_TableGenesBaixados();
				setColumnSize_TableBanco();

			}
		});
		comboIdioma.setModel(new DefaultComboBoxModel(new String[] {
				"Portugu�s", "English" }));

		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(gl_contentPane
				.createParallelGroup(Alignment.LEADING)
				.addGroup(
						gl_contentPane
								.createSequentialGroup()
								.addGap(5)
								.addComponent(tabbedPane,
										GroupLayout.DEFAULT_SIZE, 908,
										Short.MAX_VALUE).addGap(5))
				.addGroup(
						gl_contentPane
								.createSequentialGroup()
								.addContainerGap()
								.addComponent(lblGeneCollector,
										GroupLayout.PREFERRED_SIZE, 140,
										GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(button,
										GroupLayout.PREFERRED_SIZE, 35,
										GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED,
										525, Short.MAX_VALUE)
								.addComponent(lblIdiomalanguage,
										GroupLayout.PREFERRED_SIZE, 89,
										GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(comboIdioma,
										GroupLayout.PREFERRED_SIZE, 101,
										GroupLayout.PREFERRED_SIZE)
								.addContainerGap()));
		gl_contentPane
				.setVerticalGroup(gl_contentPane
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								gl_contentPane
										.createSequentialGroup()
										.addGroup(
												gl_contentPane
														.createParallelGroup(
																Alignment.LEADING)
														.addGroup(
																gl_contentPane
																		.createSequentialGroup()
																		.addGroup(
																				gl_contentPane
																						.createParallelGroup(
																								Alignment.LEADING)
																						.addComponent(
																								lblGeneCollector,
																								GroupLayout.PREFERRED_SIZE,
																								25,
																								GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								button,
																								GroupLayout.PREFERRED_SIZE,
																								27,
																								GroupLayout.PREFERRED_SIZE))
																		.addPreferredGap(
																				ComponentPlacement.RELATED))
														.addGroup(
																gl_contentPane
																		.createSequentialGroup()
																		.addContainerGap()
																		.addGroup(
																				gl_contentPane
																						.createParallelGroup(
																								Alignment.BASELINE)
																						.addComponent(
																								comboIdioma,
																								GroupLayout.PREFERRED_SIZE,
																								GroupLayout.DEFAULT_SIZE,
																								GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								lblIdiomalanguage))))
										.addPreferredGap(
												ComponentPlacement.RELATED)
										.addComponent(tabbedPane,
												GroupLayout.DEFAULT_SIZE, 543,
												Short.MAX_VALUE).addGap(6)));

		panelColetaDados = new JPanel();
		tabbedPane.addTab(idioma.getPanelColetaDados(), null, panelColetaDados,
				null);

		// Bot�o Salvar plamilha do Excel..
		bttnGerarPlanilha = new JButton(idioma.getBttnGerarPlanilha());

		// Criando o SCroolPane para poder adicionar a segunda tabela..
		scrollResultados = new JScrollPane();
		scrollResultados
				.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollResultados.setAutoscrolls(true);
		scrollResultados.setBorder(new TitledBorder(UIManager
				.getBorder("TitledBorder.border"), "Resultados da busca",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));

		// Apesar de ter o nome table_1 , essa � a segunda tabela dessa JPanel..
		tableGenesBaixados = new JTable();
		tableGenesBaixados.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

		tableGenesBaixados.setFont(new Font("Tahoma", Font.PLAIN, 13));
		tableGenesBaixados.setModel(model2);
		tableGenesBaixados.setBackground(new Color(248, 248, 255));
		tableGenesBaixados.setRowHeight(23);
		tableGenesBaixados.getTableHeader().setReorderingAllowed(false);
		setColumnSize_TableGenesBaixados();

		tableGenesBaixados.setShowGrid(false);
		tableGenesBaixados.setShowVerticalLines(false);
		tableGenesBaixados.setShowHorizontalLines(false);
		tableGenesBaixados.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("unchecked")
			@Override
			public void keyPressed(KeyEvent e) {

				switch (e.getKeyCode()) {

				case KeyEvent.VK_DELETE:

					((ExcelTableModel) tableGenesBaixados.getModel())
							.excluir(tableGenesBaixados.getSelectedRow());
					setListaPE(((ExcelTableModel) tableGenesBaixados.getModel())
							.ListGeneID());

					break;
				default:
					break;

				}
			}
		});

		tableGenesBaixados.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {

				tableGenesBaixados.getSelectionModel().clearSelection();
			}
		});

		tableGenesBaixados.setDefaultRenderer(Object.class,
				new DefaultTableCellRenderer() {
					public Component getTableCellRendererComponent(
							JTable table, Object value, boolean isSelected,
							boolean hasFocus, int row, int column) {

						super.getTableCellRendererComponent(table, value,
								isSelected, hasFocus, row, column);

						if (row % 2 == 0) {
							setBackground(new Color(245, 245, 249));
						} else {
							setBackground(new Color(237, 237, 244)); //
						}

						if (isSelected == true) {

							setForeground(new Color(255, 255, 255));
							setBackground(new Color(0, 180, 255));

						} else {
							setForeground(new Color(46, 34, 36));
						}

						return this;

					}
				});

		scrollResultados.setViewportView(tableGenesBaixados);

		btnSalvarDados = new JButton(idioma.getBtnSalvarDados());

		btnSalvarDados.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {

				// Adicionando todos os elementos que est�o na lista de genes
				// baixados para o banco de dados..
				if (bancoConecao == true) {

					try {

						for (Gene g : listaPE) {

							GeneBanco gb = new GeneBanco();
							gb.setOfficialSymbol(g.getOfficialSymbol());
							gb.setGeneId(g.getGeneId());
							gb.setOfficialName(g.getOfficialName());
							gb.setAlsoKnownAs(g.getAlsoKnownAs());
							gb.setLocationBanda(g.getLocationBanda());
							gb.setLocationCoodernada(g.getLocationCoodernada());
							gb.setTypeStrand(g.getTypeStrand());
							gb.setTranscripts(g.getTranscripts());
							gb.setGeneType(g.getGeneType());
							gb.setIdEnsembl(g.getIdEnsembl());
							gb.setReferences("");
							gb.setMetodoAnalise("");
							gb.setSexo("");
							gb.setIdade("");
							gb.setnIndividuos("");
							gb.setEndurance("");
							gb.setAgudoCronico("");
							gb.setUpDown("");
							gb.setOrganism(g.getOrganism());
							gb.setSequence(g.getSequence());
							gb.setGeneType2(g.getGeneType2());
							gb.setName(g.getName());
							gb.setDescription(g.getDescription());
							gb.setSinonyms(g.getSinonyms());

							bancoDeDados.adicionarGene(gb);
							listaBanco = (ArrayList<GeneBanco>) bancoDeDados
									.getListaGeneBanco();
							model3.setList(listaBanco);
							model3.fireTableDataChanged();

						}
						JOptionPane.showMessageDialog(null,
								idioma.getjOptionSucesso(), "",
								JOptionPane.INFORMATION_MESSAGE);
					} catch (Exception e) {
						e.printStackTrace();
						JOptionPane.showMessageDialog(null,
								idioma.getjOptionErroBdAdc(),
								idioma.getjOptionErroTitulo(),
								JOptionPane.ERROR_MESSAGE);
					}

				}

			}
		});

		btnInserirId = new JButton(idioma.getBtnInserirId());
		buttonGroup.add(btnInserirId);
		btnInserirId.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				AddGenes addGenes = new AddGenes();
				addGenes.setLocationRelativeTo(null);
				addGenes.setVisible(true);
			}
		});

		btnFornecerPlanilha = new JButton(idioma.getBtnFornecerPlanilha());
		btnFornecerPlanilha.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				ArrayList<IdGene> listXls = new CarregarXls().getLitaGeneId();
				if (listXls != null)
					listaBaixar.addAll(listXls);

			}
		});

		btnBaixarTutorial = new JButton(idioma.getBtnBaixarTutorial());
		GroupLayout gl_panelColetaDados = new GroupLayout(panelColetaDados);
		gl_panelColetaDados
				.setHorizontalGroup(gl_panelColetaDados
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								gl_panelColetaDados
										.createSequentialGroup()
										.addGap(10)
										.addGroup(
												gl_panelColetaDados
														.createParallelGroup(
																Alignment.LEADING)
														.addGroup(
																gl_panelColetaDados
																		.createSequentialGroup()
																		.addComponent(
																				btnFornecerPlanilha,
																				GroupLayout.PREFERRED_SIZE,
																				157,
																				GroupLayout.PREFERRED_SIZE)
																		.addGap(10)
																		.addComponent(
																				btnInserirId,
																				GroupLayout.PREFERRED_SIZE,
																				89,
																				GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				ComponentPlacement.UNRELATED)
																		.addComponent(
																				bttnGerarPlanilha,
																				GroupLayout.PREFERRED_SIZE,
																				157,
																				GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				ComponentPlacement.UNRELATED)
																		.addComponent(
																				btnSalvarDados,
																				GroupLayout.PREFERRED_SIZE,
																				143,
																				GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				ComponentPlacement.UNRELATED)
																		.addComponent(
																				btnBaixarTutorial,
																				GroupLayout.PREFERRED_SIZE,
																				136,
																				GroupLayout.PREFERRED_SIZE))
														.addComponent(
																scrollResultados,
																GroupLayout.DEFAULT_SIZE,
																883,
																Short.MAX_VALUE))
										.addGap(10)));
		gl_panelColetaDados
				.setVerticalGroup(gl_panelColetaDados
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								gl_panelColetaDados
										.createSequentialGroup()
										.addGap(11)
										.addGroup(
												gl_panelColetaDados
														.createParallelGroup(
																Alignment.LEADING)
														.addComponent(
																btnFornecerPlanilha)
														.addGroup(
																gl_panelColetaDados
																		.createParallelGroup(
																				Alignment.BASELINE)
																		.addComponent(
																				btnInserirId)
																		.addComponent(
																				bttnGerarPlanilha))
														.addGroup(
																gl_panelColetaDados
																		.createParallelGroup(
																				Alignment.BASELINE)
																		.addComponent(
																				btnSalvarDados)
																		.addComponent(
																				btnBaixarTutorial)))
										.addGap(11)
										.addComponent(scrollResultados,
												GroupLayout.DEFAULT_SIZE, 459,
												Short.MAX_VALUE).addGap(11)));
		panelColetaDados.setLayout(gl_panelColetaDados);
		bttnGerarPlanilha.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				// Acionando um m�todo da classe Principal.java respons�vel por
				// criar um arquivo excel..
				iniciarColeta.salvar();
			}
		});

		panelBD = new JPanel();
		tabbedPane.addTab(idioma.getPanelBD(), null, panelBD, null);

		if (bancoConecao == false)
			tabbedPane.setEnabledAt(1, false);

		panelPesquisa = new JPanel();
		panelPesquisa.setBorder(new TitledBorder(new TitledBorder(UIManager
				.getBorder("TitledBorder.border"), "", TitledBorder.LEADING,
				TitledBorder.TOP, null, null), idioma.getPanelPesquisa(),
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelPesquisa.setLayout(null);

		textField_2 = new JTextField();
		textField_2.setBounds(10, 38, 144, 20);
		panelPesquisa.add(textField_2);
		textField_2.setColumns(10);

		textIdEntrez = new JLabel(idioma.getTextIdEntrez());
		textIdEntrez.setBounds(12, 19, 46, 14);
		panelPesquisa.add(textIdEntrez);

		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(164, 38, 144, 20);
		panelPesquisa.add(textField_3);

		textIdEnsembl = new JLabel(idioma.getTextIdEnsembl());
		textIdEnsembl.setBounds(166, 19, 74, 14);
		panelPesquisa.add(textIdEnsembl);

		btnPesquisa = new JButton("");
		btnPesquisa.setIcon(new ImageIcon(FrameWork.class
				.getResource("/busca/buscar.png")));
		btnPesquisa.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {

				if (!textField_2.getText().equals("")) {

					listaBanco.clear();
					listaBanco.add(bancoDeDados.getGenePorId(
							textField_2.getText(), true));

					model3.setList(listaBanco);
					model3.fireTableDataChanged();

				} else if (!textField_3.getText().equals("")) {

					listaBanco.clear();
					listaBanco.add(bancoDeDados.getGenePorId(
							textField_3.getText(), false));

					model3.setList(listaBanco);
					model3.fireTableDataChanged();
				} else {

					listaBanco = (ArrayList<GeneBanco>) bancoDeDados
							.getListaGeneBanco();
					model3.setList(listaBanco);
					model3.fireTableDataChanged();
				}

			}
		});
		btnPesquisa.setBounds(318, 19, 56, 49);
		panelPesquisa.add(btnPesquisa);

		scrollBDLocal = new JScrollPane();
		scrollBDLocal.setBorder(new TitledBorder(UIManager
				.getBorder("TitledBorder.border"), idioma.getScrollBDLocal(),
				TitledBorder.LEADING, TitledBorder.TOP, null, null));

		tableBanco = new JTable();
		tableBanco.setShowGrid(false);
		tableBanco.setShowHorizontalLines(false);
		tableBanco.setShowVerticalLines(false);
		tableBanco.setRowHeight(23);
		tableBanco.setFont(new Font("Tahoma", Font.PLAIN, 12));
		tableBanco.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		tableBanco.setModel(model3);
		// Aplicando um estilo na tabela..
		tableBanco.setDefaultRenderer(Object.class,
				new DefaultTableCellRenderer() {
					public Component getTableCellRendererComponent(
							JTable table, Object value, boolean isSelected,
							boolean hasFocus, int row, int column) {

						super.getTableCellRendererComponent(table, value,
								isSelected, hasFocus, row, column);

						if (row % 2 == 0) {
							setBackground(new Color(245, 245, 249));
						} else {
							setBackground(new Color(237, 237, 244)); //
						}

						if (isSelected == true) {

							setForeground(new Color(255, 255, 255));
							setBackground(new Color(0, 180, 255));

						} else {
							setForeground(new Color(46, 34, 36));
						}

						return this;

					}
				});

		tableBanco.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {

				tableBanco.getSelectionModel().clearSelection();
			}
		});

		tableBanco.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {

				switch (e.getKeyCode()) {

				case KeyEvent.VK_DELETE:

					int n = tableBanco.getSelectedRow();

					pane = new JOptionPane();

					@SuppressWarnings("static-access")
					int i = pane.showOptionDialog(
							null,
							idioma.getjOptionPerg(),
							idioma.getjOptionTitulo(),
							JOptionPane.OK_CANCEL_OPTION,
							JOptionPane.INFORMATION_MESSAGE,
							null,
							new String[] { idioma.getjOptionSim(),
									idioma.getjOptionCancelar() }, // this is
																	// the array
							"default");

					if (i == 0 && n >= 0) {

						GeneBanco g = ((GeneBancoTableModel) tableBanco
								.getModel()).getGene(n);
						bancoDeDados.removerGene(g);
						listaBanco = (ArrayList<GeneBanco>) bancoDeDados
								.getListaGeneBanco();
						model3.setList(listaBanco);
						model3.fireTableDataChanged();
					}

					break;
				default:
					break;

				}
			}
		});
		
		tableBanco.setAutoCreateRowSorter(true);
		setColumnSize_TableBanco();

		scrollBDLocal.setViewportView(tableBanco);

		// Bot�o da segunda aba: Ideograma
		JButton btnIdeograma = new JButton("Ideograma");
		btnIdeograma.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				String path = getPath("txt");

				String data = build(listaBanco);

				if (!writeStringToFile(path, data)) {
					JOptionPane.showMessageDialog(null,
							"Aconteceu um erro tente novamente!");
				}else{
					JOptionPane.showMessageDialog(null,
							"Salvo com sucesso!");
				}
			}
		});

		// Bot�o da segunda aba: gerar planilha
		btnGerarPlanilha = new JButton(idioma.getBtnGerarPlanilha());
		btnGerarPlanilha.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				p = new PlanilhaBanco(listaBanco, idioma);
				try {
					p.salvarExcel();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});

		btnSalvarAltera��es = new JButton(idioma.getBtnSalvarAltera��es());
		btnSalvarAltera��es.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				try {
					for (GeneBanco g : ((GeneBancoTableModel) tableBanco
							.getModel()).getListModificaos()) {
						System.out.println(idioma.getStatus2() + g.getIdade());

						bancoDeDados.alterarGene(g);

					}

					((GeneBancoTableModel) tableBanco.getModel())
							.clearModificados();

					listaBanco = (ArrayList<GeneBanco>) bancoDeDados
							.getListaGeneBanco();
					model3.setList(listaBanco);
					model3.fireTableDataChanged();

					JOptionPane.showMessageDialog(null,
							idioma.getjOptionInfoBD(), "",
							JOptionPane.INFORMATION_MESSAGE);

				} catch (Exception e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(null,
							idioma.getjOptionErroAlterarBD(),
							idioma.getjOptionErroTitulo(),
							JOptionPane.ERROR_MESSAGE);
				}

			}
		});
		GroupLayout gl_panelBD = new GroupLayout(panelBD);
		gl_panelBD
				.setHorizontalGroup(gl_panelBD
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								gl_panelBD
										.createSequentialGroup()
										.addGroup(
												gl_panelBD
														.createParallelGroup(
																Alignment.TRAILING)
														.addGroup(
																gl_panelBD
																		.createSequentialGroup()
																		.addGap(10)
																		.addComponent(
																				panelPesquisa,
																				GroupLayout.PREFERRED_SIZE,
																				384,
																				GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				ComponentPlacement.RELATED,
																				513,
																				Short.MAX_VALUE))
														.addGroup(
																gl_panelBD
																		.createSequentialGroup()
																		.addContainerGap()
																		.addComponent(
																				btnIdeograma)
																		.addPreferredGap(
																				ComponentPlacement.UNRELATED)
																		.addComponent(
																				btnSalvarAltera��es)
																		.addPreferredGap(
																				ComponentPlacement.UNRELATED)
																		.addComponent(
																				btnGerarPlanilha)
																		.addGap(8))
														.addGroup(
																gl_panelBD
																		.createSequentialGroup()
																		.addContainerGap()
																		.addComponent(
																				scrollBDLocal,
																				GroupLayout.DEFAULT_SIZE,
																				897,
																				Short.MAX_VALUE)))
										.addContainerGap()));
		gl_panelBD
				.setVerticalGroup(gl_panelBD
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								gl_panelBD
										.createSequentialGroup()
										.addGap(11)
										.addComponent(panelPesquisa,
												GroupLayout.PREFERRED_SIZE, 75,
												GroupLayout.PREFERRED_SIZE)
										.addGap(18)
										.addComponent(scrollBDLocal,
												GroupLayout.DEFAULT_SIZE, 366,
												Short.MAX_VALUE)
										.addGap(18)
										.addGroup(
												gl_panelBD
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(
																btnIdeograma,
																GroupLayout.PREFERRED_SIZE,
																24,
																GroupLayout.PREFERRED_SIZE)
														.addComponent(
																btnSalvarAltera��es,
																GroupLayout.PREFERRED_SIZE,
																24,
																GroupLayout.PREFERRED_SIZE)
														.addComponent(
																btnGerarPlanilha,
																GroupLayout.PREFERRED_SIZE,
																24,
																GroupLayout.PREFERRED_SIZE))
										.addGap(14)));
		panelBD.setLayout(gl_panelBD);
		contentPane.setLayout(gl_contentPane);
	}

	/*
	 * m�todo statico utilizado pela classe SalvarExcel para atualizar o dados
	 * da segunda tabela da classe FrameWork sempre que a listaPE , que � a
	 * lista de Genes completa, receber um novo elemento.
	 */
	static void passarListaGene(ArrayList<Gene> lista) {

		model2 = new ExcelTableModel(lista, idioma);
		tableGenesBaixados.setModel(model2);
		// Definindo largura das colunas dessa tabela...
		tableGenesBaixados.getColumnModel().getColumn(0).setPreferredWidth(50);
		tableGenesBaixados.getColumnModel().getColumn(1).setPreferredWidth(50);
		tableGenesBaixados.getColumnModel().getColumn(2).setPreferredWidth(120);
		tableGenesBaixados.getColumnModel().getColumn(3).setPreferredWidth(350);
		tableGenesBaixados.getColumnModel().getColumn(4).setPreferredWidth(75);
		tableGenesBaixados.getColumnModel().getColumn(5).setPreferredWidth(75);
		tableGenesBaixados.getColumnModel().getColumn(6).setPreferredWidth(75);
		tableGenesBaixados.getColumnModel().getColumn(7).setPreferredWidth(100);
		tableGenesBaixados.getColumnModel().getColumn(8).setPreferredWidth(100);
		tableGenesBaixados.getColumnModel().getColumn(9).setPreferredWidth(125);
		tableGenesBaixados.getColumnModel().getColumn(10)
				.setPreferredWidth(125);
		tableGenesBaixados.getColumnModel().getColumn(11)
				.setPreferredWidth(350);
		tableGenesBaixados.getColumnModel().getColumn(12).setPreferredWidth(75);
		tableGenesBaixados.getColumnModel().getColumn(13)
				.setPreferredWidth(150);
		tableGenesBaixados.getColumnModel().getColumn(14)
				.setPreferredWidth(450);
		tableGenesBaixados.getColumnModel().getColumn(15)
				.setPreferredWidth(150);
	}

	public void setColumnSize_TableGenesBaixados() {
		// Definindo largura das colunas dessa tabela...
		tableGenesBaixados.getColumnModel().getColumn(0).setPreferredWidth(50);
		tableGenesBaixados.getColumnModel().getColumn(1).setPreferredWidth(50);
		tableGenesBaixados.getColumnModel().getColumn(2).setPreferredWidth(120);
		tableGenesBaixados.getColumnModel().getColumn(3).setPreferredWidth(350);
		tableGenesBaixados.getColumnModel().getColumn(4).setPreferredWidth(75);
		tableGenesBaixados.getColumnModel().getColumn(5).setPreferredWidth(75);
		tableGenesBaixados.getColumnModel().getColumn(6).setPreferredWidth(75);
		tableGenesBaixados.getColumnModel().getColumn(7).setPreferredWidth(100);
		tableGenesBaixados.getColumnModel().getColumn(8).setPreferredWidth(100);
		tableGenesBaixados.getColumnModel().getColumn(9).setPreferredWidth(125);
		tableGenesBaixados.getColumnModel().getColumn(10)
				.setPreferredWidth(200);
		tableGenesBaixados.getColumnModel().getColumn(11)
				.setPreferredWidth(250);
		tableGenesBaixados.getColumnModel().getColumn(12).setPreferredWidth(75);
		tableGenesBaixados.getColumnModel().getColumn(13)
				.setPreferredWidth(150);
		tableGenesBaixados.getColumnModel().getColumn(14)
				.setPreferredWidth(450);
		tableGenesBaixados.getColumnModel().getColumn(15)
				.setPreferredWidth(150);
	}

	public void setColumnSize_TableBanco() {
		// Definindo largura das colunas dessa tabela...
		tableBanco.getColumnModel().getColumn(0).setPreferredWidth(50);
		tableBanco.getColumnModel().getColumn(1).setPreferredWidth(50);
		tableBanco.getColumnModel().getColumn(2).setPreferredWidth(120);
		tableBanco.getColumnModel().getColumn(3).setPreferredWidth(350);
		tableBanco.getColumnModel().getColumn(4).setPreferredWidth(75);
		tableBanco.getColumnModel().getColumn(5).setPreferredWidth(100);
		tableBanco.getColumnModel().getColumn(6).setPreferredWidth(75);
		tableBanco.getColumnModel().getColumn(7).setPreferredWidth(75);
		tableBanco.getColumnModel().getColumn(8).setPreferredWidth(100);
		tableBanco.getColumnModel().getColumn(9).setPreferredWidth(100);
		tableBanco.getColumnModel().getColumn(10).setPreferredWidth(125);
		tableBanco.getColumnModel().getColumn(11).setPreferredWidth(150);
		tableBanco.getColumnModel().getColumn(12).setPreferredWidth(50);
		tableBanco.getColumnModel().getColumn(13).setPreferredWidth(50);
		tableBanco.getColumnModel().getColumn(14).setPreferredWidth(100);
		tableBanco.getColumnModel().getColumn(15).setPreferredWidth(100);
		tableBanco.getColumnModel().getColumn(16).setPreferredWidth(100);
		tableBanco.getColumnModel().getColumn(17).setPreferredWidth(100);
		tableBanco.getColumnModel().getColumn(18).setPreferredWidth(150);
		tableBanco.getColumnModel().getColumn(19).setPreferredWidth(300);
		tableBanco.getColumnModel().getColumn(20).setPreferredWidth(150);
		tableBanco.getColumnModel().getColumn(21).setPreferredWidth(150);
		tableBanco.getColumnModel().getColumn(22).setPreferredWidth(450);
		tableBanco.getColumnModel().getColumn(23).setPreferredWidth(150);
	}
}
