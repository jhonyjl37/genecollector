package coletor.telas;

/* Confiura��o do Idioma da Aplica��o, conforme op��o do usu�rio */
public class Idioma_Language {
	
	public Idioma_Language(){
		// Idioma padr�o: Portugu�s
		traduzir();
	}
	
	/* Elementos do Frame: FrameWork */
	private String titleFrameWork = "Gene Collector 1.1";
	// panelColetaGenes
	private String panelColetaDados;
	private String btnFornecerPlanilhaIDs;
	private String btnInserirId;
	private String btnCarregarPlanilha;
	private String bttnGerarPlanilha;
	private String btnSalvarDados;
	private String scrollResultados;
	private String tableResultados_1;
	private String tableResultados_1_1;
	private String tableResultados_2;
	private String tableResultados_3;
	private String tableResultados_4;
	private String tableResultados_5;
	private String tableResultados_5_1;
	private String tableResultados_6;
	private String tableResultados_7;
	private String tableResultados_8;
	private String tableResultados_8_1;
	private String tableResultados_9;
	private String tableResultados_9_1;
	private String tableResultados_10;
	private String tableResultados_10_1;
	private String tableResultados_11;
	private String tableResultados_12;
	private String tableResultados_13;
	private String tableResultados_14;
	private String tableResultados_15;
	private String tableResultados_15_1;
	private String tableResultados_16;
	private String tableResultados_16_1;
	private String tableResultados_17;
	private String tableResultados_17_1;
	private String tableResultados_18;
	private String tableResultados_18_1;
	private String tableResultados_19;
	private String tableResultados_20;
	private String tableResultados_21;
	private String tableResultados_21_1;
	private String tableResultados_22;
	private String tableResultados_23;
	private String tableResultados_24;
	
	// panelBD
	private String panelBD;
	private String panelPesquisa;
	private String scrollBDLocal;
	private String textIdEntrez;
	private String textIdEnsembl;
	private String btnSalvarAltera��es;
	private String btnGerarPlanilha;
	
	/* Elementos do Frame: AddGenes */
	private String titleAddGenes;
	// panel AddGenes
	private String buttonAdicionar;
	private String scrollConjuntoGenes;
	private String tableConjuntoGenes_1;
	private String tableConjuntoGenes_2;
	private String tableConjuntoGenes_3;
	private String buttonBaixarDados;
	private String buttonCancelar;
	
	/* Elementos das janelas de di�logo (JOptionPane) */
	private String status1;
	private String status2;
	private String status3;
	private String status4;
	private String jOptionPerg;
	private String jOptionTitulo;
	private String jOptionSim;
	private String jOptionCancelar;
	private String jOptionInfoBD;
	private String jOptionErroAlterarBD;
	private String jOptionErroTitulo;
	private String jOptionErroConex�oBD;
	private String jOptionSucesso;
	private String jOptionErroBdAdc;
	
	private String btnFornecerPlanilha;
	private String btnBaixarTutorial;
	
	/* Classe: CreatedDatabase.java*/
	private String tabelaCriada;
	private String erroDriverMSQL;
	private String erroConexao;
	private String outroErro;
	
	/* Classe: SoftwareInfo.java */
	// coletor.com
	private String informa��o;
	private String m�s;
	private String vers�oSoftware = "1.1";
	// coletor.telas
	private String nome;
	private String institui��o;
	
	public void traduzir() {
		/* Elementos do Frame: FrameWork */
		// panelColetaGenes
		panelColetaDados = "Coletar dados"; 
		btnInserirId = "Inserir IDs";
		btnCarregarPlanilha = "Anexar Planilha"; 
		bttnGerarPlanilha = "Gerar Planilha "; 
		btnSalvarDados = "Salvar dados (BD)"; 
		scrollResultados = "Resultados da busca";
		tableResultados_1 = "<html>S�mbolo<br />Oficial</html>";
		tableResultados_1_1 = "S�mbolo Oficial";
		tableResultados_2 = "ID Gene";
		tableResultados_3 = "Tamb�m chamado de ";
		tableResultados_4 = "Nome completo Oficial";
		tableResultados_5 = "<html>Localiza��o <br />(Entrez)</html>";
		tableResultados_5_1 = "Localiza��o (Entrez)";
		tableResultados_6 = "Refer�ncias";
		tableResultados_7 = "Transcritos";
		tableResultados_8 = "<html>Localiza��o <br />(Ensembl)</html>";
		tableResultados_8_1 = "Localiza��o (Ensembl)";
		tableResultados_9 = "Fita";
		tableResultados_9_1 = tableResultados_9;
		tableResultados_10 = "<html>Tipo de Gene <br />(Ensembl)</html>";
		tableResultados_10_1 = "Tipo de Gene (Ensembl)";
		tableResultados_11 = "ID Ensembl";
		tableResultados_12 = "M�todo de An�lise";
		tableResultados_13 = "Sexo";
		tableResultados_14 = "Idade";
		tableResultados_15 = "<html>N� de <br />Indiv�duos</html>";
		tableResultados_15_1 = "N� de Indiv�duos";
		tableResultados_16 = "<html>Resist�ncia/<br />Res/Misto</html>";
		tableResultados_16_1 = "Resist�ncia/Res/Misto";
		tableResultados_17 = "<html>Agudo/<br />Cr�nico</html>";
		tableResultados_17_1 = "Agudo/Cr�nico";
		tableResultados_18 = "<html>Superior/<br />inferior</html>";
		tableResultados_18_1 = "Superior/inferior";
		tableResultados_19 = "Organismo";
		tableResultados_20 = "Sequ�ncia";
		tableResultados_21 = "<html>Tipo do Gene <br />(Ensembl)</html>";
		tableResultados_21_1 = "Tipo do Gene (Ensembl)";
		tableResultados_22 = "Nome";
		tableResultados_23 = "Descri��o";
		tableResultados_24 = "Sin�nimos";

		// panelBD
		panelBD = "Banco de Dados"; 
		panelPesquisa = "Pesquisa";
		scrollBDLocal = "Banco de Dados Local";
		textIdEntrez = "ID Entrez";
		textIdEnsembl = "ID Ensembl"; 
		btnSalvarAltera��es = "Salvar Altera��es"; 
		btnGerarPlanilha = "Gerar Planilha";

		// panelAddGenes
		titleAddGenes = "Adicionar Genes";
		buttonAdicionar = "Adicionar";
		scrollConjuntoGenes = "Conjunto de Genes";
		tableConjuntoGenes_1 = "ID Entrez";
		tableConjuntoGenes_2 = "ID Ensembl";
		tableConjuntoGenes_3 = "Status";
		buttonBaixarDados = "Baixar dados";
		buttonCancelar = "Cancelar";
		
		/* Elementos das janelas de di�logo (JOptionPane) */
		status1 = "Baixando...";
		status2 = "Alterando..."; 
		status3 = "Conclu�do";
		status4 = "Erro";
		jOptionPerg = "Voc� deseja realmente deletar esse Gene?"; 
		jOptionTitulo = "Deletar Gene"; 
		jOptionSim = "Sim"; 
		jOptionCancelar = "Cancelar"; 
		jOptionInfoBD = "O Banco de dados foi alterado com sucesso!"; 
		jOptionErroAlterarBD = "N�o foi poss�vel alterar os dados."; 
		jOptionErroTitulo = "Erro"; 
		jOptionErroConex�oBD = "N�o foi poss�vel fazer a conex�o com o Banco de Dados local"; 
		jOptionSucesso = "Genes adicionados com sucesso no Banco de Dados local!"; 
		jOptionErroBdAdc = "N�o foi poss�vel adicionar os genes no Banco de Dados local";
		
		btnFornecerPlanilha = "Fornecer Planilha de IDs"; 
		btnBaixarTutorial = "Baixar Tutorial"; 
		
		/* Classe: CreatedDatabase.java*/
		tabelaCriada = "tabela criada";
		erroDriverMSQL = "Erro: falha ao tentar carregar driver do MySQL.";
		erroConexao = "Erro: falha ao tentar criar uma conex�o com o objeto";
		outroErro = "Outro erro: ";
		
		/* Classe: SoftwareInfo.java */
		// coletor.com
		informa��o = "Criado pela Nambus Software - " +
			"Empresa J�nior da Universidade Federal Rural do Semi-�rido/RN (UFERSA/RN) campus Angicos." +
			"\r\n\r\nnambu.software@ufersa.edu.br\r\n\r\n";
		m�s = "Mar�o";
		
		// coletor.telas
		nome = "Nome";
		institui��o = "Institui��o";
	}
	
	public void translate(){
		
		// Elementos do panelColetaGenes
		setPanelColetaDados("Collect data");
		btnInserirId = "Insert IDs";
		btnCarregarPlanilha = "Attach Spreadsheet";
		bttnGerarPlanilha = "Generate spreadsheet";
		btnSalvarDados = "Save Data (DB)";
		scrollResultados = "Search results ";
		tableResultados_1 = "<html>Official <br />Symbol<html>";
		tableResultados_1_1 = "Official Symbol";
		tableResultados_2 = "Gene ID";
		tableResultados_3 = "Also Known as";
		tableResultados_4 = "Official Full Name";
		tableResultados_5 = "<html>Location <br />(banda)</html>";
		tableResultados_5_1 = "Location (banda)";
		tableResultados_6 = "References";
		tableResultados_7 = "Transcripts";
		tableResultados_8 = "<html>Location <br />(co-ordinates)</html>";
		tableResultados_8_1 = "Location (co-ordinates)";
		tableResultados_9 = "<html>Forward/<br />Reverse <br />strand</html>";
		tableResultados_9_1 = "Forward/Reverse strand";
		tableResultados_10 = "Gene Type";
		tableResultados_11 = "Ensembl ID";
		tableResultados_12 = "Analysis Method";
		tableResultados_13 = "Gender";
		tableResultados_14 = "Age";
		tableResultados_15 = "<html>Number of <br />individuals<html>";
		tableResultados_15_1 = "Number of individuals";
		tableResultados_16 = "<html>Endurance/<br />Res/Mixed<html>";
		tableResultados_16_1 = "Endurance/Res/Mixed";
		tableResultados_17 = "<html>Pointed/<br />Chronic</html>";
		tableResultados_17_1 = "Pointed/Chronic";
		tableResultados_18 = "Up/down";
		tableResultados_18_1 = tableResultados_18;
		tableResultados_19 = "Organism";
		tableResultados_20 = "Sequence";
		tableResultados_21 = "<html>Gene Type <br />(Ensembl)</html>";
		tableResultados_21_1 = "Gene Type (Ensembl)";
		tableResultados_22 = "Name";
		tableResultados_23 = "Description";
		tableResultados_24 = "Sinonyms";
		
		// panelBD
		panelBD = "Database";
		panelPesquisa = "Search";
		scrollBDLocal = "Local Database";
		textIdEntrez = "Entrez ID";
		textIdEnsembl = "Ensembl ID";
		btnSalvarAltera��es = "Save changes";
		btnGerarPlanilha = "Generate spreadsheet ";
		
		// panelAddGenes
		titleAddGenes = "Add Gene";
		buttonAdicionar = "Add";
		scrollConjuntoGenes = "Gene set";
		tableConjuntoGenes_1 = "Entrez ID";
		tableConjuntoGenes_2 = "Ensembl ID";
		tableConjuntoGenes_3 = "Status";
		buttonBaixarDados = "Download data";
		buttonCancelar = "Cancel";
		
		/* Elementos das janelas de di�logo (JOptionPane) */
		status1 = "Downloading...";
		status2 = "Altering...";
		status3 = "Completed";
		status4 = "Error";
		jOptionPerg = "Do you really  want to delete this Gene?";
		jOptionTitulo = "Delete Gene";
		jOptionSim = "Yes";
		jOptionCancelar = "Cancel";
		jOptionInfoBD = "The database was changed successfully!";
		jOptionErroAlterarBD = "It was not possible to change the data";
		jOptionErroTitulo = "Error";
		jOptionErroConex�oBD = "It was not possible connecting with the local database";
		jOptionSucesso = "Genes added successfully in local database!";

		btnFornecerPlanilha = "Providing IDs Worksheet";
		btnBaixarTutorial = "Download Tutorial";		
		
		/* Classe: CreatedDatabase.java*/
		tabelaCriada = "table created";
		erroDriverMSQL = "Error: failed to load MySQL driver.";
		erroConexao = "Error: failed to create a connection object.";
		outroErro = "Other error: ";
		
		/* Classe: SoftwareInfo.java */
		// coletor.com
		informa��o = "Created by Nambus Software - " +
		"Junior Enterprise from Universidade Federal Rural do Semi-�rido/RN (UFERSA/RN) campus Angicos." +
		"\r\n\r\nnambu.software@ufersa.edu.br\r\n\r\n";
		m�s = "March";
		
		//coletor.telas
		nome = "Name";
		institui��o = "Institution";
	}
	
	/**
	 * @return the titleFrameWork
	 */
	public String getTitleFrameWork() {
		return titleFrameWork;
	}

	/**
	 * @param titleFrameWork the titleFrameWork to set
	 */
	public void setTitleFrameWork(String titleFrameWork) {
		this.titleFrameWork = titleFrameWork;
	}

	/**
	 * @return the panelColetaGenes
	 */
	public String getPanelColetaDados() {
		return panelColetaDados;
	}
	/**
	 * @param panelColetaGenes the panelColetaGenes to set
	 */
	public void setPanelColetaDados(String panelColetaDados) {
		this.panelColetaDados = panelColetaDados;
	}
	/**
	 * @return the btnFornecerPlanilhaIDs
	 */
	public String getBtnFornecerPlanilhaIDs() {
		return btnFornecerPlanilhaIDs;
	}
	/**
	 * @param btnFornecerPlanilhaIDs the btnFornecerPlanilhaIDs to set
	 */
	public void setBtnFornecerPlanilhaIDs(String btnFornecerPlanilhaIDs) {
		this.btnFornecerPlanilhaIDs = btnFornecerPlanilhaIDs;
	}
	/**
	 * @return the btnInserirId
	 */
	public String getBtnInserirId() {
		return btnInserirId;
	}

	/**
	 * @param btnInserirId the btnInserirId to set
	 */
	public void setBtnInserirId(String btnInserirId) {
		this.btnInserirId = btnInserirId;
	}

	/**
	 * @return the btnCarregarPlanilha
	 */
	public String getBtnCarregarPlanilha() {
		return btnCarregarPlanilha;
	}
	/**
	 * @param btnCarregarPlanilha the btnCarregarPlanilha to set
	 */
	public void setBtnCarregarPlanilha(String btnCarregarPlanilha) {
		this.btnCarregarPlanilha = btnCarregarPlanilha;
	}
	/**
	 * @return the bttnGerarPlanilha
	 */
	public String getBttnGerarPlanilha() {
		return bttnGerarPlanilha;
	}
	/**
	 * @param bttnGerarPlanilha the bttnGerarPlanilha to set
	 */
	public void setBttnGerarPlanilha(String bttnGerarPlanilha) {
		this.bttnGerarPlanilha = bttnGerarPlanilha;
	}
	/**
	 * @return the titleAddGenes
	 */
	public String getTitleAddGenes() {
		return titleAddGenes;
	}

	/**
	 * @param titleAddGenes the titleAddGenes to set
	 */
	public void setTitleAddGenes(String titleAddGenes) {
		this.titleAddGenes = titleAddGenes;
	}

	/**
	 * @return the btnSalvarDados
	 */
	public String getBtnSalvarDados() {
		return btnSalvarDados;
	}
	/**
	 * @param btnSalvarDados the btnSalvarDados to set
	 */
	public void setBtnSalvarDados(String btnSalvarDados) {
		this.btnSalvarDados = btnSalvarDados;
	}
	/**
	 * @return the scrollResultados
	 */
	public String getScrollResultados() {
		return scrollResultados;
	}
	/**
	 * @param scrollResultados the scrollResultados to set
	 */
	public void setScrollResultados(String scrollResultados) {
		this.scrollResultados = scrollResultados;
	}
	/**
	 * @return the tableResultados_1
	 */
	public String getTableResultados_1() {
		return tableResultados_1;
	}
	/**
	 * @param tableResultados_1 the tableResultados_1 to set
	 */
	public void setTableResultados_1(String tableResultados_1) {
		this.tableResultados_1 = tableResultados_1;
	}
	/**
	 * @return the tableResultados_2
	 */
	public String getTableResultados_2() {
		return tableResultados_2;
	}
	/**
	 * @param tableResultados_2 the tableResultados_2 to set
	 */
	public void setTableResultados_2(String tableResultados_2) {
		this.tableResultados_2 = tableResultados_2;
	}
	/**
	 * @return the tableResultados_3
	 */
	public String getTableResultados_3() {
		return tableResultados_3;
	}
	/**
	 * @param tableResultados_3 the tableResultados_3 to set
	 */
	public void setTableResultados_3(String tableResultados_3) {
		this.tableResultados_3 = tableResultados_3;
	}
	/**
	 * @return the tableResultados_4
	 */
	public String getTableResultados_4() {
		return tableResultados_4;
	}
	/**
	 * @param tableResultados_4 the tableResultados_4 to set
	 */
	public void setTableResultados_4(String tableResultados_4) {
		this.tableResultados_4 = tableResultados_4;
	}
	/**
	 * @return the tableResultados_5
	 */
	public String getTableResultados_5() {
		return tableResultados_5;
	}
	/**
	 * @param tableResultados_5 the tableResultados_5 to set
	 */
	public void setTableResultados_5(String tableResultados_5) {
		this.tableResultados_5 = tableResultados_5;
	}
	/**
	 * @return the tableResultados_6
	 */
	public String getTableResultados_6() {
		return tableResultados_6;
	}
	/**
	 * @param tableResultados_6 the tableResultados_6 to set
	 */
	public void setTableResultados_6(String tableResultados_6) {
		this.tableResultados_6 = tableResultados_6;
	}
	/**
	 * @return the tableResultados_7
	 */
	public String getTableResultados_7() {
		return tableResultados_7;
	}
	/**
	 * @param tableResultados_7 the tableResultados_7 to set
	 */
	public void setTableResultados_7(String tableResultados_7) {
		this.tableResultados_7 = tableResultados_7;
	}
	/**
	 * @return the tableResultados_8
	 */
	public String getTableResultados_8() {
		return tableResultados_8;
	}
	/**
	 * @param tableResultados_8 the tableResultados_8 to set
	 */
	public void setTableResultados_8(String tableResultados_8) {
		this.tableResultados_8 = tableResultados_8;
	}
	/**
	 * @return the tableResultados_9
	 */
	public String getTableResultados_9() {
		return tableResultados_9;
	}
	/**
	 * @param tableResultados_9 the tableResultados_9 to set
	 */
	public void setTableResultados_9(String tableResultados_9) {
		this.tableResultados_9 = tableResultados_9;
	}
	/**
	 * @return the tableResultados_10
	 */
	public String getTableResultados_10() {
		return tableResultados_10;
	}
	/**
	 * @param tableResultados_10 the tableResultados_10 to set
	 */
	public void setTableResultados_10(String tableResultados_10) {
		this.tableResultados_10 = tableResultados_10;
	}
	/**
	 * @return the tableResultados_11
	 */
	public String getTableResultados_11() {
		return tableResultados_11;
	}

	/**
	 * @param tableResultados_11 the tableResultados_11 to set
	 */
	public void setTableResultados_11(String tableResultados_11) {
		this.tableResultados_11 = tableResultados_11;
	}

	/**
	 * @return the tableResultados_12
	 */
	public String getTableResultados_12() {
		return tableResultados_12;
	}

	/**
	 * @param tableResultados_12 the tableResultados_12 to set
	 */
	public void setTableResultados_12(String tableResultados_12) {
		this.tableResultados_12 = tableResultados_12;
	}

	/**
	 * @return the tableResultados_13
	 */
	public String getTableResultados_13() {
		return tableResultados_13;
	}

	/**
	 * @param tableResultados_13 the tableResultados_13 to set
	 */
	public void setTableResultados_13(String tableResultados_13) {
		this.tableResultados_13 = tableResultados_13;
	}

	/**
	 * @return the tableResultados_14
	 */
	public String getTableResultados_14() {
		return tableResultados_14;
	}

	/**
	 * @param tableResultados_14 the tableResultados_14 to set
	 */
	public void setTableResultados_14(String tableResultados_14) {
		this.tableResultados_14 = tableResultados_14;
	}

	/**
	 * @return the tableResultados_15
	 */
	public String getTableResultados_15() {
		return tableResultados_15;
	}

	/**
	 * @param tableResultados_15 the tableResultados_15 to set
	 */
	public void setTableResultados_15(String tableResultados_15) {
		this.tableResultados_15 = tableResultados_15;
	}

	/**
	 * @return the tableResultados_16
	 */
	public String getTableResultados_16() {
		return tableResultados_16;
	}

	/**
	 * @param tableResultados_16 the tableResultados_16 to set
	 */
	public void setTableResultados_16(String tableResultados_16) {
		this.tableResultados_16 = tableResultados_16;
	}

	/**
	 * @return the tableResultados_17
	 */
	public String getTableResultados_17() {
		return tableResultados_17;
	}

	/**
	 * @param tableResultados_17 the tableResultados_17 to set
	 */
	public void setTableResultados_17(String tableResultados_17) {
		this.tableResultados_17 = tableResultados_17;
	}

	/**
	 * @return the tableResultados_18
	 */
	public String getTableResultados_18() {
		return tableResultados_18;
	}

	/**
	 * @param tableResultados_18 the tableResultados_18 to set
	 */
	public void setTableResultados_18(String tableResultados_18) {
		this.tableResultados_18 = tableResultados_18;
	}

	/**
	 * @return the tableResultados_1_1
	 */
	public String getTableResultados_1_1() {
		return tableResultados_1_1;
	}

	/**
	 * @param tableResultados_1_1 the tableResultados_1_1 to set
	 */
	public void setTableResultados_1_1(String tableResultados_1_1) {
		this.tableResultados_1_1 = tableResultados_1_1;
	}

	/**
	 * @return the tableResultados_5_1
	 */
	public String getTableResultados_5_1() {
		return tableResultados_5_1;
	}

	/**
	 * @param tableResultados_5_1 the tableResultados_5_1 to set
	 */
	public void setTableResultados_5_1(String tableResultados_5_1) {
		this.tableResultados_5_1 = tableResultados_5_1;
	}

	/**
	 * @return the tableResultados_8_1
	 */
	public String getTableResultados_8_1() {
		return tableResultados_8_1;
	}

	/**
	 * @param tableResultados_8_1 the tableResultados_8_1 to set
	 */
	public void setTableResultados_8_1(String tableResultados_8_1) {
		this.tableResultados_8_1 = tableResultados_8_1;
	}

	/**
	 * @return the tableResultados_9_1
	 */
	public String getTableResultados_9_1() {
		return tableResultados_9_1;
	}

	/**
	 * @param tableResultados_9_1 the tableResultados_9_1 to set
	 */
	public void setTableResultados_9_1(String tableResultados_9_1) {
		this.tableResultados_9_1 = tableResultados_9_1;
	}

	/**
	 * @return the tableResultados_10_1
	 */
	public String getTableResultados_10_1() {
		return tableResultados_10_1;
	}

	/**
	 * @param tableResultados_10_1 the tableResultados_10_1 to set
	 */
	public void setTableResultados_10_1(String tableResultados_10_1) {
		this.tableResultados_10_1 = tableResultados_10_1;
	}

	/**
	 * @return the tableResultados_15_1
	 */
	public String getTableResultados_15_1() {
		return tableResultados_15_1;
	}

	/**
	 * @param tableResultados_15_1 the tableResultados_15_1 to set
	 */
	public void setTableResultados_15_1(String tableResultados_15_1) {
		this.tableResultados_15_1 = tableResultados_15_1;
	}

	/**
	 * @return the tableResultados_16_1
	 */
	public String getTableResultados_16_1() {
		return tableResultados_16_1;
	}

	/**
	 * @param tableResultados_16_1 the tableResultados_16_1 to set
	 */
	public void setTableResultados_16_1(String tableResultados_16_1) {
		this.tableResultados_16_1 = tableResultados_16_1;
	}

	/**
	 * @return the tableResultados_17_1
	 */
	public String getTableResultados_17_1() {
		return tableResultados_17_1;
	}

	/**
	 * @param tableResultados_17_1 the tableResultados_17_1 to set
	 */
	public void setTableResultados_17_1(String tableResultados_17_1) {
		this.tableResultados_17_1 = tableResultados_17_1;
	}

	/**
	 * @return the tableResultados_18_1
	 */
	public String getTableResultados_18_1() {
		return tableResultados_18_1;
	}

	/**
	 * @param tableResultados_18_1 the tableResultados_18_1 to set
	 */
	public void setTableResultados_18_1(String tableResultados_18_1) {
		this.tableResultados_18_1 = tableResultados_18_1;
	}

	/**
	 * @return the tableResultados_19
	 */
	public String getTableResultados_19() {
		return tableResultados_19;
	}

	/**
	 * @param tableResultados_19 the tableResultados_19 to set
	 */
	public void setTableResultados_19(String tableResultados_19) {
		this.tableResultados_19 = tableResultados_19;
	}

	/**
	 * @return the tableResultados_20
	 */
	public String getTableResultados_20() {
		return tableResultados_20;
	}

	/**
	 * @param tableResultados_20 the tableResultados_20 to set
	 */
	public void setTableResultados_20(String tableResultados_20) {
		this.tableResultados_20 = tableResultados_20;
	}

	/**
	 * @return the tableResultados_21
	 */
	public String getTableResultados_21() {
		return tableResultados_21;
	}

	/**
	 * @param tableResultados_21 the tableResultados_21 to set
	 */
	public void setTableResultados_21(String tableResultados_21) {
		this.tableResultados_21 = tableResultados_21;
	}

	/**
	 * @return the tableResultados_21_1
	 */
	public String getTableResultados_21_1() {
		return tableResultados_21_1;
	}

	/**
	 * @param tableResultados_21_1 the tableResultados_21_1 to set
	 */
	public void setTableResultados_21_1(String tableResultados_21_1) {
		this.tableResultados_21_1 = tableResultados_21_1;
	}

	/**
	 * @return the tableResultados_22
	 */
	public String getTableResultados_22() {
		return tableResultados_22;
	}

	/**
	 * @param tableResultados_22 the tableResultados_22 to set
	 */
	public void setTableResultados_22(String tableResultados_22) {
		this.tableResultados_22 = tableResultados_22;
	}

	/**
	 * @return the tableResultados_23
	 */
	public String getTableResultados_23() {
		return tableResultados_23;
	}

	/**
	 * @param tableResultados_23 the tableResultados_23 to set
	 */
	public void setTableResultados_23(String tableResultados_23) {
		this.tableResultados_23 = tableResultados_23;
	}

	/**
	 * @return the tableResultados_24
	 */
	public String getTableResultados_24() {
		return tableResultados_24;
	}

	/**
	 * @param tableResultados_24 the tableResultados_24 to set
	 */
	public void setTableResultados_24(String tableResultados_24) {
		this.tableResultados_24 = tableResultados_24;
	}

	/**
	 * @return the panelBD
	 */
	public String getPanelBD() {
		return panelBD;
	}
	/**
	 * @param panelBD the panelBD to set
	 */
	public void setPanelBD(String panelBD) {
		this.panelBD = panelBD;
	}
	/**
	 * @return the panelPesquisa
	 */
	public String getPanelPesquisa() {
		return panelPesquisa;
	}
	/**
	 * @param panelPesquisa the panelPesquisa to set
	 */
	public void setPanelPesquisa(String panelPesquisa) {
		this.panelPesquisa = panelPesquisa;
	}
	/**
	 * @return the scrollBDLocal
	 */
	public String getScrollBDLocal() {
		return scrollBDLocal;
	}
	/**
	 * @param scrollBDLocal the scrollBDLocal to set
	 */
	public void setScrollBDLocal(String scrollBDLocal) {
		this.scrollBDLocal = scrollBDLocal;
	}
	public String getTextIdEntrez() {
		return textIdEntrez;
	}

	public void setTextIdEntrez(String textIdEntrez) {
		this.textIdEntrez = textIdEntrez;
	}

	public String getTextIdEnsembl() {
		return textIdEnsembl;
	}

	public void setTextIdEnsembl(String textIdEnsembl) {
		this.textIdEnsembl = textIdEnsembl;
	}

	/**
	 * @return the btnSalvarAltera��es
	 */
	public String getBtnSalvarAltera��es() {
		return btnSalvarAltera��es;
	}
	/**
	 * @param btnSalvarAltera��es the btnSalvarAltera��es to set
	 */
	public void setBtnSalvarAltera��es(String btnSalvarAltera��es) {
		this.btnSalvarAltera��es = btnSalvarAltera��es;
	}
	/**
	 * @return the btnGerarPlanilha
	 */
	public String getBtnGerarPlanilha() {
		return btnGerarPlanilha;
	}
	/**
	 * @param btnGerarPlanilha the btnGerarPlanilha to set
	 */
	public void setBtnGerarPlanilha(String btnGerarPlanilha) {
		this.btnGerarPlanilha = btnGerarPlanilha;
	}
	/**
	 * @return the buttonAdicionar
	 */
	public String getButtonAdicionar() {
		return buttonAdicionar;
	}
	/**
	 * @param buttonAdicionar the buttonAdicionar to set
	 */
	public void setButtonAdicionar(String buttonAdicionar) {
		this.buttonAdicionar = buttonAdicionar;
	}
	/**
	 * @return the scrollConjuntoGenes
	 */
	public String getScrollConjuntoGenes() {
		return scrollConjuntoGenes;
	}
	/**
	 * @param scrollConjuntoGenes the scrollConjuntoGenes to set
	 */
	public void setScrollConjuntoGenes(String scrollConjuntoGenes) {
		this.scrollConjuntoGenes = scrollConjuntoGenes;
	}
	/**
	 * @return the tableConjuntoGenes_1
	 */
	public String getTableConjuntoGenes_1() {
		return tableConjuntoGenes_1;
	}

	/**
	 * @param tableConjuntoGenes_1 the tableConjuntoGenes_1 to set
	 */
	public void setTableConjuntoGenes_1(String tableConjuntoGenes_1) {
		this.tableConjuntoGenes_1 = tableConjuntoGenes_1;
	}

	/**
	 * @return the tableConjuntoGenes_2
	 */
	public String getTableConjuntoGenes_2() {
		return tableConjuntoGenes_2;
	}

	/**
	 * @param tableConjuntoGenes_2 the tableConjuntoGenes_2 to set
	 */
	public void setTableConjuntoGenes_2(String tableConjuntoGenes_2) {
		this.tableConjuntoGenes_2 = tableConjuntoGenes_2;
	}

	/**
	 * @return the tableConjuntoGenes_3
	 */
	public String getTableConjuntoGenes_3() {
		return tableConjuntoGenes_3;
	}

	/**
	 * @param tableConjuntoGenes_3 the tableConjuntoGenes_3 to set
	 */
	public void setTableConjuntoGenes_3(String tableConjuntoGenes_3) {
		this.tableConjuntoGenes_3 = tableConjuntoGenes_3;
	}

	/**
	 * @return the buttonBaixarDados
	 */
	public String getButtonBaixarDados() {
		return buttonBaixarDados;
	}
	/**
	 * @param buttonBaixarDados the buttonBaixarDados to set
	 */
	public void setButtonBaixarDados(String buttonBaixarDados) {
		this.buttonBaixarDados = buttonBaixarDados;
	}
	/**
	 * @return the buttonCancelar
	 */
	public String getButtonCancelar() {
		return buttonCancelar;
	}
	/**
	 * @param buttonCancelar the buttonCancelar to set
	 */
	public void setButtonCancelar(String buttonCancelar) {
		this.buttonCancelar = buttonCancelar;
	}

	/**
	 * @return the status1
	 */
	public String getStatus1() {
		return status1;
	}

	/**
	 * @param status1 the status1 to set
	 */
	public void setStatus1(String status1) {
		this.status1 = status1;
	}

	/**
	 * @return the status2
	 */
	public String getStatus2() {
		return status2;
	}

	/**
	 * @param status2 the status2 to set
	 */
	public void setStatus2(String status2) {
		this.status2 = status2;
	}

	/**
	 * @return the status3
	 */
	public String getStatus3() {
		return status3;
	}

	/**
	 * @param status3 the status3 to set
	 */
	public void setStatus3(String status3) {
		this.status3 = status3;
	}

	/**
	 * @return the status4
	 */
	public String getStatus4() {
		return status4;
	}

	/**
	 * @param status4 the status4 to set
	 */
	public void setStatus4(String status4) {
		this.status4 = status4;
	}

	/**
	 * @return the jOptionPerg
	 */
	public String getjOptionPerg() {
		return jOptionPerg;
	}

	/**
	 * @param jOptionPerg the jOptionPerg to set
	 */
	public void setjOptionPerg(String jOptionPerg) {
		this.jOptionPerg = jOptionPerg;
	}

	/**
	 * @return the jOptionTitulo
	 */
	public String getjOptionTitulo() {
		return jOptionTitulo;
	}

	/**
	 * @param jOptionTitulo the jOptionTitulo to set
	 */
	public void setjOptionTitulo(String jOptionTitulo) {
		this.jOptionTitulo = jOptionTitulo;
	}

	/**
	 * @return the jOptionSim
	 */
	public String getjOptionSim() {
		return jOptionSim;
	}

	/**
	 * @param jOptionSim the jOptionSim to set
	 */
	public void setjOptionSim(String jOptionSim) {
		this.jOptionSim = jOptionSim;
	}

	/**
	 * @return the jOptionCancelar
	 */
	public String getjOptionCancelar() {
		return jOptionCancelar;
	}

	/**
	 * @param jOptionCancelar the jOptionCancelar to set
	 */
	public void setjOptionCancelar(String jOptionCancelar) {
		this.jOptionCancelar = jOptionCancelar;
	}

	/**
	 * @return the jOptionInfoBD
	 */
	public String getjOptionInfoBD() {
		return jOptionInfoBD;
	}

	/**
	 * @param jOptionInfoBD the jOptionInfoBD to set
	 */
	public void setjOptionInfoBD(String jOptionInfoBD) {
		this.jOptionInfoBD = jOptionInfoBD;
	}

	/**
	 * @return the jOptionErroAlterarBD
	 */
	public String getjOptionErroAlterarBD() {
		return jOptionErroAlterarBD;
	}

	/**
	 * @param jOptionErroAlterarBD the jOptionErroAlterarBD to set
	 */
	public void setjOptionErroAlterarBD(String jOptionErroAlterarBD) {
		this.jOptionErroAlterarBD = jOptionErroAlterarBD;
	}

	/**
	 * @return the jOptionErroTitulo
	 */
	public String getjOptionErroTitulo() {
		return jOptionErroTitulo;
	}

	/**
	 * @param jOptionErroTitulo the jOptionErroTitulo to set
	 */
	public void setjOptionErroTitulo(String jOptionErroTitulo) {
		this.jOptionErroTitulo = jOptionErroTitulo;
	}

	/**
	 * @return the jOptionErroConex�oBD
	 */
	public String getjOptionErroConex�oBD() {
		return jOptionErroConex�oBD;
	}

	/**
	 * @param jOptionErroConex�oBD the jOptionErroConex�oBD to set
	 */
	public void setjOptionErroConex�oBD(String jOptionErroConex�oBD) {
		this.jOptionErroConex�oBD = jOptionErroConex�oBD;
	}

	/**
	 * @return the jOptionSucesso
	 */
	public String getjOptionSucesso() {
		return jOptionSucesso;
	}

	/**
	 * @param jOptionSucesso the jOptionSucesso to set
	 */
	public void setjOptionSucesso(String jOptionSucesso) {
		this.jOptionSucesso = jOptionSucesso;
	}

	/**
	 * @return the jOptionErroBdAdc
	 */
	public String getjOptionErroBdAdc() {
		return jOptionErroBdAdc;
	}

	/**
	 * @param jOptionErroBdAdc the jOptionErroBdAdc to set
	 */
	public void setjOptionErroBdAdc(String jOptionErroBdAdc) {
		this.jOptionErroBdAdc = jOptionErroBdAdc;
	}

	/**
	 * @return the btnFornecerPlanilha
	 */
	public String getBtnFornecerPlanilha() {
		return btnFornecerPlanilha;
	}

	/**
	 * @param btnFornecerPlanilha the btnFornecerPlanilha to set
	 */
	public void setBtnFornecerPlanilha(String btnFornecerPlanilha) {
		this.btnFornecerPlanilha = btnFornecerPlanilha;
	}

	/**
	 * @return the btnBaixarTutorial
	 */
	public String getBtnBaixarTutorial() {
		return btnBaixarTutorial;
	}

	/**
	 * @param btnBaixarTutorial the btnBaixarTutorial to set
	 */
	public void setBtnBaixarTutorial(String btnBaixarTutorial) {
		this.btnBaixarTutorial = btnBaixarTutorial;
	}

	/**
	 * @return the tabelaCriada
	 */
	public String getTabelaCriada() {
		return tabelaCriada;
	}

	/**
	 * @param tabelaCriada the tabelaCriada to set
	 */
	public void setTabelaCriada(String tabelaCriada) {
		this.tabelaCriada = tabelaCriada;
	}

	/**
	 * @return the erroDriverMSQL
	 */
	public String getErroDriverMSQL() {
		return erroDriverMSQL;
	}

	/**
	 * @param erroDriverMSQL the erroDriverMSQL to set
	 */
	public void setErroDriverMSQL(String erroDriverMSQL) {
		this.erroDriverMSQL = erroDriverMSQL;
	}

	/**
	 * @return the erroConexao
	 */
	public String getErroConexao() {
		return erroConexao;
	}

	/**
	 * @param erroConexao the erroConexao to set
	 */
	public void setErroConexao(String erroConexao) {
		this.erroConexao = erroConexao;
	}

	/**
	 * @return the outroErro
	 */
	public String getOutroErro() {
		return outroErro;
	}

	/**
	 * @param outroErro the outroErro to set
	 */
	public void setOutroErro(String outroErro) {
		this.outroErro = outroErro;
	}

	/**
	 * @return the informa��o
	 */
	public String getInforma��o() {
		return informa��o;
	}

	/**
	 * @param informa��o the informa��o to set
	 */
	public void setInforma��o(String informa��o) {
		this.informa��o = informa��o;
	}

	/**
	 * @return the m�s
	 */
	public String getM�s() {
		return m�s;
	}

	/**
	 * @param m�s the m�s to set
	 */
	public void setM�s(String m�s) {
		this.m�s = m�s;
	}

	/**
	 * @return the vers�oSoftware
	 */
	public String getVers�oSoftware() {
		return vers�oSoftware;
	}

	/**
	 * @param vers�oSoftware the vers�oSoftware to set
	 */
	public void setVers�oSoftware(String vers�oSoftware) {
		this.vers�oSoftware = vers�oSoftware;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * @return the institui��o
	 */
	public String getInstitui��o() {
		return institui��o;
	}

	/**
	 * @param institui��o the institui��o to set
	 */
	public void setInstitui��o(String institui��o) {
		this.institui��o = institui��o;
	}
}
