package coletor.ideograma;

public class Instance {
	private String officiaSymbol;
	private String banda;
	private String coordinatesLeft;
	private String coordinatesRight;
	private String strand;
	
	public String getOfficiaSymbol() {
		return officiaSymbol;
	}
	public void setOfficiaSymbol(String officiaSymbol) {
		this.officiaSymbol = officiaSymbol;
	}
	public String getBanda() {
		return banda;
	}
	public void setBanda(String banda) {
		this.banda = banda;
	}
	public String getCoordinatesLeft() {
		return coordinatesLeft;
	}
	public void setCoordinatesLeft(String coordinatesLeft) {
		this.coordinatesLeft = coordinatesLeft;
	}
	public String getCoordinatesRight() {
		return coordinatesRight;
	}
	public void setCoordinatesRight(String coordinatesRight) {
		this.coordinatesRight = coordinatesRight;
	}
	public String getStrand() {
		return strand;
	}
	public void setStrand(String strand) {
		this.strand = strand;
	}

}
