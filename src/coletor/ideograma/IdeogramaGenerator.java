package coletor.ideograma;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import coletor.banco.GeneBanco;

public class IdeogramaGenerator {

	public static String build(ArrayList<GeneBanco> lista) {
		String out = "\n";
		String t, f = "";

		int homoSapiens = 0;
		int rattusNorvegicus = 0;
		int musMusculus = 0;
		int drosophilaMelanogaster = 0;

		String orga;
		for (GeneBanco gene : lista) {
			orga = gene.getOrganism();

			if (orga.toLowerCase().contains("homo")
					&& orga.toLowerCase().contains("sapiens"))
				homoSapiens++;

			if (orga.toLowerCase().contains("rattus")
					&& orga.toLowerCase().contains("norvegicus"))
				rattusNorvegicus++;

			if (orga.toLowerCase().contains("mus")
					&& orga.toLowerCase().contains("musculus"))
				musMusculus++;

			if (orga.toLowerCase().contains("drosophila")
					&& orga.toLowerCase().contains("melanogaster"))
				drosophilaMelanogaster++;
		}

		String first = "", second = "";
		if (homoSapiens > rattusNorvegicus && homoSapiens > musMusculus
				&& homoSapiens > drosophilaMelanogaster) {
			first = "homo";
			second = "sapiens";
		}

		if (rattusNorvegicus > homoSapiens && rattusNorvegicus > musMusculus
				&& rattusNorvegicus > drosophilaMelanogaster) {
			first = "rattus";
			second = "norvegicus";
		}

		if (musMusculus > homoSapiens && rattusNorvegicus > musMusculus
				&& musMusculus > drosophilaMelanogaster) {
			first = "mus";
			second = "musculus";
		}

		if (drosophilaMelanogaster > homoSapiens
				&& drosophilaMelanogaster > musMusculus
				&& drosophilaMelanogaster > rattusNorvegicus) {
			first = "drosophila";
			second = "melanogaster";
		}

		for (GeneBanco gene : lista) {
			if (compareOrganism(first, second, gene.getOrganism())) {
				out += "mapping    17    black ";
				t = gene.getOfficialSymbol();
				out += t;
				out += sp(8 - t.length()) + " .    ";
				t = getBandaParser(gene.getLocationBanda());
				out += "chr" + t;
				f = getCoordinates(gene.getLocationCoodernada());
				out += sp(9 - t.length()) + " " + f;
				out += sp(16 - f.length()) + " "
						+ getStrand(gene.getTypeStrand());
				out += "\n";
			}
		}

		return out;
	}

	private static String getBandaParser(String s) {

		s = s.replaceAll(" ", "");

		Pattern pattern = Pattern.compile("^.*?(?=[a-zA-Z])");
		Matcher match = pattern.matcher(s);

		if (match.find()) {
			return match.group().toString();
		} else {
			return "";
		}
	}

	private static String getCoordinates(String s) {

		s = s.replaceAll(" ", "");
		s = s.replaceAll(",", "");
		String left = "";
		String right = "";

		Pattern pattern = Pattern.compile("^.*?(?=-)");
		Matcher match = pattern.matcher(s);

		if (match.find()) {
			left = match.group().toString();
		}

		pattern = Pattern.compile("(?<=-).*");
		match = pattern.matcher(s);
		if (match.find()) {
			right = match.group().toString();
		}

		return left + sp(8 - left.length()) + right;
	}

	private static String getStrand(String s) {
		s = s.replaceAll(" ", "");
		s = s.toLowerCase();

		if (s.contains("forward")) {
			return "+";
		} else {
			return "-";
		}
	}

	private static String sp(int n) {
		String t = "";
		for (int i = 0; i <= n; i++)
			t += " ";

		return t;
	}

	private static boolean compareOrganism(String first, String second,
			String text) {
		if (text.toLowerCase().contains(first)
				&& text.toLowerCase().contains(second)) {
			return true;
		} else {
			return false;
		}

	}

	public static void main(String[] args) {
		System.out.println("Started \n test Brand");
		System.out.println(getBandaParser("723sdD24.1."));

		System.out.println("test Coordinates ");
		System.out.println(getCoordinates("117,105,838-117,356,025"));

		System.out.println("test strand ");
		System.out.println(getStrand("Forward"));

	}

}
